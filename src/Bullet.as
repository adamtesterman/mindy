﻿package {
	//IMPORTS
	import flash.display.MovieClip;
	
	import chars.*;
	
	public class Bullet extends MovieClip {
		//PROPERTIES / Variables
		private var bulletType:String;
		private var startX:int;					//tracks starting location, for weapon ranges
		private var startY:int;
		private var optCounter:int;				//optimization counter (prevents certain tasks from being performed every single frame)
		public var pcDidShoot:Boolean;
		public var dx:Number;					//velocities
		public var dy:Number;
		public var speed:Number;
		public var damage:int;
		public var range:int;
		public var piercing:Boolean;
		public var friendlyFire:Boolean;		//can hurt anyone except shooter
		
		//PROPERTIES / Objects
		private var gameRoot:MovieClip;			//reference to Main
		private var myParent:MovieClip;			//display parent
		private var myShooter:MovieClip;
		
		
		//METHODS / Constructor
		public function Bullet(rootRef:MovieClip, displayParent:MovieClip, shooter:MovieClip, targetX:int, targetY:int) {
			//New properties
			gameRoot = rootRef;
			myParent = displayParent;
			myShooter = shooter;
			
			bulletType = myShooter.weapon;
			damage = myShooter.weaponDamage;
			speed = myShooter.weaponBulletSpeed;
			range = myShooter.weaponBulletRange;
			piercing = myShooter.weaponBulletPierces;
			
			if (myShooter == gameRoot.pc) pcDidShoot = true;
			
			//Inherited methods
			myParent.addChildAt(this, myParent.getChildIndex(shooter));
			
			//Inherited properties
			x = startX = myShooter.x;
			y = startY = myShooter.y;
			
			//New methods
			SetVelocities(targetX, targetY);
			
			//Inherited methods
			gotoAndStop(bulletType);
			
			//Type-specific setup
			if 		(myShooter is PC) 		gameRoot.pcBullets.push(this);
			else if (myShooter is Enemy) 	gameRoot.enemyBullets.push(this);
		}
		
		
		//METHODS / Destroy Self
		public function killMe() {
			//Remove bullet from tracking arrays based on type of bullet
			if 		(gameRoot.pcBullets.indexOf(this) > -1) 		gameRoot.pcBullets.splice(gameRoot.pcBullets.indexOf(this), 1);
			else if (gameRoot.enemyBullets.indexOf(this) > -1) 		gameRoot.enemyBullets.splice(gameRoot.enemyBullets.indexOf(this), 1);
			myParent.removeChild(this);
		}
		
		
		//Set velocities based on target location
		private function SetVelocities(tX:int, tY:int) {
			var _rotDX:int = tX - x;							//get relative target location
			var _rotDY:int = tY - y;
			var _cursorAngle = Math.atan2(_rotDY, _rotDX);		//set angle based on that
			rotation = 360 * (_cursorAngle / (2 * Math.PI));
			
			dx = Math.cos(Math.PI * rotation / 180) * speed;
			dy = Math.sin(Math.PI * rotation / 180) * speed;
		}
		
		//Update position based on AI settings and velocities
		public function pulse() {
			optCounter++;
			
			//Move object based on velocities
			x += dx;
			y += dy;
			
			//Update alpha and distance traveled if this bullet has a limited range
			if (range > 0) {
				if (gameRoot.getDistanceToPoint(x, y, startX, startY) > range) { killMe(); return; }	//if beyond range, destroy self
				else alpha = 1 - ((gameRoot.getDistanceToPoint(x, y, startX, startY) / range) - .3);	//otherwise, continue fading out based on overall distance traveled
			}
			
			DetectCollisions();	//FACTOR OPT COUNTER IN TO PIERCING WEAPON DAMAGE
		}
		
		//Detect and react to collisions with characters, walls, and being offscreen
		private function DetectCollisions() {
			//OPTIMIZATION CHECK
			if (optCounter % 3 == 0) {
				//PC bullets and friendly fire bullets against enemies
				if (myShooter is PC || myShooter.confused) {
					for (var i:int = gameRoot.enemies.length - 1; i >= 0; i--) {
						//Make sure not to test against shooter
						if ((gameRoot.enemies[i].torso && hitTestObject(gameRoot.enemies[i].torso)) && (!myShooter.confused || (myShooter.confused && gameRoot.enemies[i] != myShooter))) {
							if (!piercing) killMe();
							gameRoot.modifyUnitHealth(gameRoot.enemies[i], -(int(damage * gameRoot.enemies[i].defense)), false, myShooter);
							
							return;
						}
					}
				}
				
				//Enemy bullets against PC and partner
				else if (myShooter is Enemy && gameRoot.gameState == gameRoot.GAME_PLAYING) {
					for (i = gameRoot.enemyBullets.length - 1; i >= 0; i--) {
						if (hitTestObject(gameRoot.pc.torso)) {
							if (!piercing) killMe();
							gameRoot.modifyUnitHealth(gameRoot.pc, -(int(damage * gameRoot.pc.defense)), true, myShooter);
							gameRoot.pcDamagedThisWave = true;
							
							return;
						}
						else if (!gameRoot.partner.dead && hitTestObject(gameRoot.partner.torso)) {
							if (!piercing) killMe();
							gameRoot.modifyUnitHealth(gameRoot.partner, -(Math.ceil(damage * gameRoot.partner.defense * .4)), true, myShooter);	//partner takes 40% of all damage, calculated after defense is taken into account
							
							return;
						}
					}
				}
				
				//Destroy if off the board
				if ((hitTestObject(myParent.wall1)) ||
					(hitTestObject(myParent.wall2)) ||
					(hitTestObject(myParent.wall3)) ||
					(hitTestObject(myParent.wall4))) {
						killMe();
				}
			}
		}
	}
}
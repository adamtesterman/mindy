﻿package {
	import flash.display.MovieClip;
	import flash.events.*;
	
	import chars.*;
	import gamescreens.*;
	
	import utils.caurina.transitions.Tweener;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class Item extends MovieClip	{
		//PARAMETERS / Constants
		public static const WEAPON_MAYHEM_CANNON:String =				"mayhemcannon";
		public static const WEAPON_FLAMETHROWER:String =				"flamethrower";
		public static const WEAPON_GUMBALL_BLASTER:String =				"gumballblaster";
		public static const WEAPON_BUSTERS:String =						"busters";
		public static const WEAPON_REPEATER:String =					"repeater";
		
		public static const SKILL_CRYBABY:String =						"crybaby";
		public static const SKILL_BRAT:String =							"brat";
		
		public static const ITEM_HEALTH_10:String =						"health10";
		public static const ITEM_HEALTH_25:String =						"health25";
		public static const ITEM_HEALTH_40:String =						"health40";
		public static const ITEM_HEALTH_50:String =						"health50";
		
		public static const INVENTORY_GROUP_WEAPONS_MINDY:String =		"mindyWeaponSlot";
		public static const INVENTORY_GROUP_WEAPONS_BINKY:String =		"binkyWeaponSlot";
		public static const INVENTORY_GROUP_SKILLS_MINDY:String =		"mindySkillSlot";
		public static const INVENTORY_GROUP_SKILLS_BINKY:String =		"binkySkillSlot";
		public static const INVENTORY_GROUP_ITEMS:String =				"item";
		
		public const WEAPON:int =										0;					//item type labels
		public const SKILL:int =										1;
		public const ITEM:int =											2;
		
		//PARAMETERS / Variables
		public var itemName:String;
		public var itemType:int;
		public var inventoryGroup:String;
		
		//PARAMETERS / Objects
		protected var gameRoot:MovieClip;		//reference to Main
		protected var myParent:MovieClip;		//display parent
		
		
		//METHODS / Constructor
		public function Item(item:String = null) {
			//Inherited properties
			selectionBox.visible = false;
			grayOut.visible = false;
			
			//Inherited methods
			if (item) { itemName = item;  gotoAndStop(itemName); }
			else stop();
			addEventListener(Event.ADDED_TO_STAGE, HandleAddedToStage);
			addEventListener(MouseEvent.MOUSE_OVER, HandleMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, HandleMouseOut);
			
			//Type-specifice setup
			if 		(itemName == Item.WEAPON_MAYHEM_CANNON) { 	itemType = WEAPON;	inventoryGroup = Item.INVENTORY_GROUP_WEAPONS_MINDY; 	}
			else if (itemName == Item.WEAPON_FLAMETHROWER) 	{	itemType = WEAPON;	inventoryGroup = Item.INVENTORY_GROUP_WEAPONS_MINDY; 	}
			else if (itemName == Item.WEAPON_GUMBALL_BLASTER){	itemType = WEAPON;	inventoryGroup = Item.INVENTORY_GROUP_WEAPONS_MINDY;	}
			else if (itemName == Item.WEAPON_BUSTERS)		{	itemType = WEAPON;	inventoryGroup = Item.INVENTORY_GROUP_WEAPONS_BINKY; 	}
			else if (itemName == Item.WEAPON_REPEATER)		{	itemType = WEAPON;	inventoryGroup = Item.INVENTORY_GROUP_WEAPONS_BINKY; 	}
			else if (itemName == Item.SKILL_CRYBABY)		{	itemType = SKILL;	inventoryGroup = Item.INVENTORY_GROUP_SKILLS_MINDY;		}
			else if (itemName == Item.SKILL_BRAT)			{	itemType = SKILL;	inventoryGroup = Item.INVENTORY_GROUP_SKILLS_MINDY;		}
			else if (itemName == Item.ITEM_HEALTH_10 || itemName == Item.ITEM_HEALTH_25 || itemName == Item.ITEM_HEALTH_40 || itemName == Item.ITEM_HEALTH_50) {
				bg.visible = false;
				
				itemType = ITEM;
				inventoryGroup = Item.INVENTORY_GROUP_ITEMS;
				
				alpha = 0;
				scaleY = .01;
				
				Tweener.addTween(this, { alpha:1, time:.25, transition:"linear" } );
				Tweener.addTween(this, { scaleY:1, time:.75, transition:"easeOutElastic" } );
			}
		}
		
		
		//METHODS / Deconstructor
		public function killMe() {
			RemoveEventListeners();
			if (parent) parent.removeChild(this);
		}
		
		//Allow this item to be clicked, have mouse states, etc.
		public function activateButtonMode() {
			buttonMode = true;
			
			addEventListener(MouseEvent.CLICK, SelectMe);
			
			//Immediately put a selection box around the characters' currently equipped weapon
			if (gameRoot.pc.weapon == currentLabel || gameRoot.partner.weapon == currentLabel) SelectMe();
		}
		
		//Deactivate this item by disallowing it to be clicked, have mouse states, etc.
		public function deactivateButtonMode() {
			buttonMode = false;
			
			removeEventListener(MouseEvent.CLICK, SelectMe);
			
			selectionBox.visible = false;
		}
		
		//Get root ref after being added to stage
		private function HandleAddedToStage(event:Event) {
			removeEventListener(Event.ADDED_TO_STAGE, HandleAddedToStage);
			
			gameRoot = MovieClip(root);
		}
		
		//Show tooltip for this item
		private function HandleMouseOver(event:MouseEvent = null) {
			if (currentLabel && bg.visible) ShowTooltip();	//only show tooltip if this is a non-blank item slot
		}
		
		//Hide tooltip
		private function HandleMouseOut(event:MouseEvent = null) {
			gameRoot.tooltip.visible = false;
		}
		
		//Remove all event listeners
		private function RemoveEventListeners() {
			removeEventListener(Event.ADDED_TO_STAGE, HandleAddedToStage);
			removeEventListener(MouseEvent.MOUSE_OVER, HandleMouseOver);
			removeEventListener(MouseEvent.MOUSE_OUT, HandleMouseOut);
			removeEventListener(MouseEvent.CLICK, SelectMe);
		}
		
		//Highlight this item while making sure all other items in this category are not highlighted
		private function SelectMe(event:MouseEvent = null) {
			//De-select all other items in this category
			for (var i:int = 0; i < parent.numChildren; i++) {
				if (parent.getChildAt(i) is Item && Item(parent.getChildAt(i)).inventoryGroup == inventoryGroup) Item(parent.getChildAt(i)).selectionBox.visible = false;
			}
			
			selectionBox.visible = true;
			
			if 		(inventoryGroup == Item.INVENTORY_GROUP_WEAPONS_MINDY) 	gameRoot.mindy.equipWeapon(itemName);
			else if (inventoryGroup == Item.INVENTORY_GROUP_WEAPONS_BINKY) 	gameRoot.binky.equipWeapon(itemName);
			else if (inventoryGroup == Item.INVENTORY_GROUP_SKILLS_MINDY) {
				//If the use was successful, remove the item from the inventory
				if (gameRoot.mindy.activateSkill(itemName)) {
					deactivateButtonMode();
					gotoAndStop(1);
					InventoryDialog(parent).UpdateIconCooldownBoxes();
				}
			}
			else if (inventoryGroup == Item.INVENTORY_GROUP_SKILLS_BINKY) {
				//If the use was successful, remove the item from the inventory
				if (gameRoot.binky.activateSkill(itemName)) {
					deactivateButtonMode();
					gotoAndStop(1);
					InventoryDialog(parent).UpdateIconCooldownBoxes();
				}
			}
		}
		
		//Show tooltip for this item and make sure the entire tooltip is onscreen
		private function ShowTooltip() {
			gameRoot.tooltip.visible = true;
			gameRoot.tooltip.x = stage.mouseX;
			gameRoot.tooltip.y = stage.mouseY;
			gameRoot.tooltip.gotoAndStop(currentLabel);
			gameRoot.addChild(gameRoot.tooltip);		//keep on top
			
			//Prevent tooltip from going offscreen
			if 		(gameRoot.tooltip.x + gameRoot.tooltip.width > gameRoot.RIGHT_EDGE) gameRoot.tooltip.x -= gameRoot.tooltip.width;
			else if (gameRoot.tooltip.x < 0) gameRoot.tooltip.x += gameRoot.tooltip.width;
		}
	}
}
package {
	import flash.display.MovieClip;
	import flash.events.Event;
	
	import utils.caurina.transitions.Tweener;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class GoreExplosion extends MovieClip {
		//PROPERTIES / Constants
		private const MAX_EXPLODE_DISTANCE:int =		100;				//the furthest that any piece of gore can travel from its starting location
		private const TWEEN_TRANSITION_TYPE:String =	"easeOutExpo";
		private const NUM_SKULLS:int =					1;
		private const NUM_BONES:int =					4;
		private const NUM_BLOOD_DROPS:int =				20;
		
		//PROPERTIES / Objects
		private var gameRoot:MovieClip;										//reference to Main
		private var myParent:MovieClip;										//display parent
		
		//PROPERTIES / Lists
		private var goreList:Array = [];									//list of all pieces of gore in this explosion
		private var finishedGoreList:Array = [];							//list of all pieces of gore that have reached their destination; once this list is full, all gore will fade out at the same time
		
		
		//METHODS / Constructor
		public function GoreExplosion(rootRef:MovieClip, displayParent:MovieClip, startX:int, startY:int) {
			//New properties
			gameRoot = rootRef;
			myParent = displayParent;
			
			//Inherited properties
			x = startX;
			y = startY;
			
			//New methods
			CreateGore();
			SetGoreDestinations();
			
			//Inherited methods
			myParent.addChildAt(this, 1);
		}
		
		
		//METHODS / Deconstructor
		public function killMe() {
			RemoveEventListeners();
			
			for (var i:int = goreList.length; i >= 0; i--) {
				goreList[i] = null;
				finishedGoreList[i] = null;
			}
			goreList = null;
			finishedGoreList = null;
			
			myParent.removeChild(this);
		}
		
		
		//METHODS / Remove All Event Listeners
		private function RemoveEventListeners() {
			removeEventListener(Event.ENTER_FRAME, CheckForGore);
		}
		
		
		//Add a piece of gore to the list of pieces that are finished moving, and check to see if all are finished
		private function AddFinishedGorePiece(piece:Fader) {
			finishedGoreList.push(piece);
			
			if (finishedGoreList.length == goreList.length) FadeOutAllGore();
		}
		
		//If no more gore exists (all have faded out) destroy self
		private function CheckForGore(event:Event) {
			if (numChildren < 2) killMe();
		}
		
		//Create the bits and chunks that will fly
		private function CreateGore() {
			var _newSkull:Fader;
			var _newBone:Fader;
			var _newBlood:Fader;
			var i:int;
			
			//Create 1 skull
			for (i = 0; i < NUM_SKULLS; i++) {
				_newSkull = new Fader(gameRoot, this, Fader.FADER_SKULL, 0, 0);
				_newSkull.rotation = Math.random() * 360;
				goreList.push(_newSkull);
			}
			
			
			//Create 4 bones
			for (i = 0; i < NUM_BONES; i++) {
				_newBone = new Fader(gameRoot, this, Fader.FADER_BONE, 0, 0);
				_newBone.rotation = Math.random() * 360;
				goreList.push(_newBone);
			}
			
			//Create 20 drops of blood at varying sizes
			for (i = 0; i < NUM_BLOOD_DROPS; i++) {
				_newBlood = new Fader(gameRoot, this, Fader.FADER_BLOOD, 0, 0);
				_newBlood.scaleX = _newBlood.scaleY = Math.random() * 1.5 + .5;		//half scale to 2x scale
				goreList.push(_newBlood);
			}
		}
		
		//Fade out every piece of gore at the same time, and remove all of them when they're done fading
		private function FadeOutAllGore() {
			for (var i:int = 0; i < goreList.length; i++) {
				goreList[i].dAlpha = -.05;
			}
			
			//Constantly look to see if all gore is faded out, then destroy self
			addEventListener(Event.ENTER_FRAME, CheckForGore);
		}
		
		//Set the end point for each piece of gore
		private function SetGoreDestinations() {
			for (var i:int = 0; i < goreList.length; i++) {
				goreList[i].destinationX = Math.random() * MAX_EXPLODE_DISTANCE;
				goreList[i].destinationY = Math.random() * MAX_EXPLODE_DISTANCE;
				
				//Each piece can go either left or right, up or down
				if (Math.random() < .5) goreList[i].destinationX *= -1;
				if (Math.random() < .5) goreList[i].destinationY *= -1;
				
				//trace(goreList[i].destinationX, goreList[i].destinationY);
				
				//Give the piece of gore a tween so it can move to its destination
				Tweener.addTween(goreList[i], { x:goreList[i].destinationX,
												y:goreList[i].destinationY,
												rotation:-goreList[i].rotation,
												time:Math.random() + .5,
												transition:TWEEN_TRANSITION_TYPE,
												onComplete:function() { AddFinishedGorePiece(goreList[i]); } } );
			}
		}
	}
}
﻿//Adam Testerman
//Michael Stokes
//Matthew Russell
//Tony Manning

package {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.ui.Mouse;
	
	import chars.*;
	import gamescreens.*;
	
	import utils.SoundController;
	
	import utils.caurina.transitions.Tweener;
	
	public class Main extends MovieClip {
		//PROPERTIES / Constants
		public const RIGHT_EDGE:int =						640;	//game dimensions
		public const BOTTOM_EDGE:int =						480;
		
		public const TOP_LEFT:Point = 						new Point(-RIGHT_EDGE / 2, 				-BOTTOM_EDGE / 2);		//positions for game screens
		public const TOP_MIDDLE:Point = 					new Point( RIGHT_EDGE / 2, 				-BOTTOM_EDGE / 2);
		public const TOP_RIGHT:Point = 						new Point( RIGHT_EDGE + RIGHT_EDGE / 2, -BOTTOM_EDGE / 2);
		public const MIDDLE_LEFT:Point = 					new Point(-RIGHT_EDGE / 2, 				 BOTTOM_EDGE / 2);
		public const MIDDLE_MIDDLE:Point = 					new Point( RIGHT_EDGE / 2, 				 BOTTOM_EDGE / 2);
		public const MIDDLE_RIGHT:Point =					new Point( RIGHT_EDGE + RIGHT_EDGE / 2,  BOTTOM_EDGE / 2);
		public const BOTTOM_LEFT:Point = 					new Point(-RIGHT_EDGE / 2, 				 BOTTOM_EDGE + BOTTOM_EDGE / 2);
		public const BOTTOM_MIDDLE:Point = 					new Point( RIGHT_EDGE / 2, 				 BOTTOM_EDGE + BOTTOM_EDGE / 2);
		public const BOTTOM_RIGHT:Point = 					new Point( RIGHT_EDGE + RIGHT_EDGE / 2,  BOTTOM_EDGE + BOTTOM_EDGE / 2);
		
		public const GAME_PLAYING:int =						1;		//game state labels
		public const GAME_PAUSED:int =						2;
		public const GAME_OVER:int =						3;
		
		public const PC_MINDY:int =							1;		//pc label
		public const PC_BINKY:int =							2;
		
		public const ENEMY_RADISH:int =						1;		//enemy labels
		public const ENEMY_BROCCOLI:int =					2;
		public const ENEMY_BRUSSELS:int =					3;
		public const ENEMY_CORN:int =						4;
		public const ENEMY_EGGPLANT:int =					5;
		public const ENEMY_CAULI:int =						6;
		public const ENEMY_PEPPER:int =						7;
		public const ENEMY_SPINACH:int =					8;
		public const ENEMY_SQUASH:int =						9;
		public const ENEMY_MUSHROOM:int =					10;
		public const ENEMY_FLOWER_CLOWN:int =				11;
		public const ENEMY_MIME:int =						12;
		public const ENEMY_LION:int =						13;
		public const ENEMY_FIREBLOWER:int =					14;
		public const ENEMY_SNEAKY_CLOWN:int =				15;
		public const ENEMY_JUGGLING_CLOWN:int =				16;
		public const ENEMY_VENTRILOQUIST:int =				17;
		public const ENEMY_EVIL_BALLOON:int =				18;
		public const ENEMY_DEVIOUS_CLOWN:int =				19;
		public const ENEMY_RINGLEADER:int =					20;
		
		private const DREAM_GATE_Y:int =					-520;
		
		private const ENERGY_ABSORB_PERCENT:Number =		.05;			//how much hp damage is converted to energy, as a percent (5%)
		
		private const NP_MULT_FLAWLESS_WAVE:Number =		.10;			//np added to np earned this wave if pc took no damage during this wave
		private const NP_MULT_INC_KILL_ENEMY:Number =		.05;			//np that the kill enemy multiplier is incremented every time a wave is completed (additive)
		
		private const MAX_ONSCREEN_ENEMIES:int =			20;
		
		public const GLOBAL_RECAST_TIMER_MAX:int =			1800;			//1 min global nightmare skill cooldown
		
		//PROPERTIES / Variables
		public var gameState:int;
		public var numGameScreens:int;						//counter for number of game screens currently visible; used to determine whether or not game should be unpaused when any given game screen is closed
		
		public var north:Boolean;							//key press flags
		public var south:Boolean;
		public var east:Boolean;
		public var west:Boolean;
		
		public var campaign:int = 1;
		public var currentWaveIndex:int;
		
		public var np:int;									//Nightmare Points (game's currency)
		public var npThisWave:int;
		public var npMultKillEnemy:Number = 	.95;		//percentage of np added from each enemy, updated with successive wave completions (reset upon death and shop visit)
		public var pcDamagedThisWave:Boolean;				//Is used to drive the bonus NP per wave for no damage taken
		
		private var spawnDelayTimerMax:int = 30;
		private var spawnDelayTimerCur:int = spawnDelayTimerMax;
		
		private var healthDropDelayTimerMax:int = 450;		//30 sec
		private var healthDropDelayTimerCur:int = healthDropDelayTimerMax;
		
		public var globalRecastTimerCur:int;
		public var pauseEnemySpawns:Boolean;
		
		//PROPERTIES / Objects
		public var pc:PC;									//currently controlled character
		public var partner:PC;								//non-controlled partner
		
		public var mindy:PC;								//absolute reference to a character, regardless of its pc/partner status
		public var binky:PC;
		
		private var crossHair:CrossHair;
		public var dreamGate:DreamGate;
		
		//PROPERTIES / Lists
		private var enemyWaves:Array = [];
		private var currentWave:Array = [];
		public var unlockedWaves:Array = [];
		public var campaignNPRequirements:Array = [];
		public var enemies:Array = [];
		public var pcBullets:Array = [];
		public var enemyBullets:Array = [];
		public var items:Array = [];						//list of items currently held by the player
		
		//PROPERTIES / Music
		public var bgmTitle:Sound;
		public var bgmLevel01:Sound;
		public var bgmLevel02:Sound;
		
		//PROPERTIES / Sound effects
		//public var sfxThing:Sound;
		
		//PROPERTIES / Voice Overs
		public var voiStory01:Sound;
		
		
		//METHODS / Constructor
		public function Main() {
			unlockedWaves[0] = [0];		//only the first wave is unlocked at the start
			unlockedWaves[1] = [];
			unlockedWaves[2] = [];
			
			//unlockedWaves[0] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];		//TESTING PURPOSES
			//unlockedWaves[1] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
			
			campaignNPRequirements[0] = 0;		//np required to unlock a specific campaign
			campaignNPRequirements[1] = 3000;
			campaignNPRequirements[2] = 5000;
			
			//New methods
			FillWaveList();
			
			//Inherited methods
			stop();
			addEventListener(Event.ENTER_FRAME, PreloadGame);
		}
		
		
		//METHODS / Start/Restart Wave
		public function startWave(waveNum:int, fromWaveSelect:Boolean) {
			//New properties
			currentWaveIndex = waveNum;
			if (currentWaveIndex > 9) {
				currentWaveIndex = 0;
				campaign++;
			}
			
			currentWave = [];								//empty current wave and refill it
			for (var i:int = 0; i < enemyWaves[currentWaveIndex].length; i++) {
				currentWave.push(enemyWaves[currentWaveIndex][i]);
			}
			
			npThisWave = 0;									//reset np gained this wave
			npMultKillEnemy += NP_MULT_INC_KILL_ENEMY;		//add to enemy np multiplier
			
			pcDamagedThisWave = false;						//reset damaged flag
			spawnDelayTimerCur = spawnDelayTimerMax;		//reset spawn delay timer
			north = south = east = west = false;			//reset key press flags (prevents a bug)
			
			//Inherited properties
			if (fromWaveSelect) {
				level.x = RIGHT_EDGE / 2;					//re-center the level, but only if starting the wave from wave select
				level.y = BOTTOM_EDGE / 2;
			}
			level.gotoAndStop(campaign);					//make sure level is on correct campaign
			screenBlocker.visible = false;
			hud.txtWave.text = (currentWaveIndex + 1).toString();
			
			//New methods
			if (fromWaveSelect) pc.reset();			//reset positions and nightmare skill effects of pc and partner, and revive partner if needed, but do not reset weapons
			if (fromWaveSelect) partner.reset();
			partner.revive();								//bring partner back from death either way
			if (currentWaveIndex == 9) CreateDreamGate();	//create dream gate if needed
			
			//Show story if needed
			if (campaign == 1 && currentWaveIndex == 0) {
				var _storyDialog:StoryDialog = new StoryDialog();
				_storyDialog.gameRoot = this;
				addChild(_storyDialog);
			}
			else {
				setGameState(GAME_PLAYING);						//add appropriate game event listeners
				SoundController.playMusic(this["bgmLevel0" + campaign]);
				
				stage.focus = stage;
			}
		}
		
		
		//METHODS / Main Game Loop
		private function GameLoop(event:Event) {
			UpdateCrossHair();
			
			SpawnHealthDrop();
			if (currentWave.length > 0) SpawnEnemyWave();
			
			if (globalRecastTimerCur > 0) globalRecastTimerCur--;
			
			pc.pulse();
			partner.pulse();
			PulseEnemies();
			PulseBullets();
			
			SortAllByDepth();
		}
		
		
		//METHODS / Remove Everything From Everywhere
		public function killAll() {
			//Tweener.removeAllTweens();	//works for every single object? "removes all tweens from the engine." <-- doc
		}
		
		
		//METHODS / Preload Game
		private function PreloadGame(event:Event) {
			//IF FULLY LOADED, START GAME
			var _bytesLoaded:int = loaderInfo.bytesLoaded;
			var _bytesTotal:int = loaderInfo.bytesTotal;
			
			//Convert to KB
			//
			
			//Update progress bar
			progressBar.scaleX = _bytesLoaded / _bytesTotal;
			
			if (_bytesLoaded >= _bytesTotal) {
				removeEventListener(Event.ENTER_FRAME, PreloadGame);
				addFrameScript(1, InitGame);
				gotoAndStop("main");
			}
		}
		
		
		//METHODS / Initialize Game Content
		private function InitGame() {
			CreateSounds();
			pc = new PC(this, level, PC_MINDY);			//create pc and partner
			partner = new PC(this, level, PC_BINKY);
			partner.y = -50;
			
			mindy = pc;
			binky = partner;
			
			var _titleScreen:TitleScreen = new TitleScreen(this, this, MIDDLE_MIDDLE);
			SoundController.playMusic(bgmTitle);
		}
		
		//Add all event listeners that are required for gameplay
		private function AddGameEventListeners() {
			addEventListener(Event.ENTER_FRAME, GameLoop);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, function() { pc.shooting = true; });
			stage.addEventListener(MouseEvent.MOUSE_UP, function() { pc.shooting = false; });
		}
		
		//Check for no more enemies and whether or not wave has been depleted
		public function checkWaveComplete() {
			//If there are no enemies left onscreen or ready to be spawned...
			if (enemies.length == 0 && currentWave.length == 0) {
				//If Dream Gate doesn't exist, just complete the wave
				if (!dreamGate) HandleWaveComplete();						//if not on a dream gate wave, complete the wave
				//Otherwise, wait for PC to touch the Dream Gate
				else if (dreamGate && pc.hitTestObject(dreamGate)) {		//otherwise, handle Dream Gate functionality
					var _dreamGateDialog:DreamGateDialog = new DreamGateDialog(this, this, BOTTOM_MIDDLE, 0);	//otherwise, show dream gate dialog
				}
			}
		}
		
		//Create a dream gate for this campaign
		private function CreateDreamGate() {
			dreamGate = new DreamGate();
			dreamGate.y = DREAM_GATE_Y;
			dreamGate.stop();
			level.addChild(dreamGate);
		}
		
		//Instantiate sound objects
		private function CreateSounds() {
			bgmTitle = new		BGMTitle();
			bgmLevel01 = new 	BGMLevel01();
			bgmLevel02 = new	BGMLevel02();
			
			voiStory01 = new	voiStory();
		}
		
		//Remove an item from a character's inventory
		public function discardItem(item:String, char:MovieClip) {
			if 		(char.invWeapons && char.invWeapons.indexOf(item) > -1) char.invWeapons.splice(char.invWeapons.indexOf(item), 1);
			else if (char.invSkills && char.invSkills.indexOf(item) > -1) 	char.invSkills.splice(char.invSkills.indexOf(item), 1);
			else if (char.invItems && char.invItems.indexOf(item) > -1)		char.invItems.splice(char.invItems.indexOf(item), 1);
		}
		
		//Setup wave format
		private function FillWaveList() {
			//UP TO 20 PER LINE
			enemyWaves[0] =	[	1, 	1, 	1, 	1, 	1,	1, 	1, 	1, 	1, 	1	];	//10x (#1)
								
			enemyWaves[1] =	[	1, 	1, 	1, 	1, 	1,	1, 	1, 	1, 	1, 	1, 
								2, 	2, 	2, 	2, 	2						];	//10x (#1), 5x (#2)
								
			enemyWaves[2] =	[	1, 	1, 	1, 	1, 	1, 	1, 	1, 	1, 	1, 	1,
								3, 	3, 	3, 	3, 	3,	3, 	3, 	3, 	3, 	3	];	//10x (#1), 10x (#3)
								
			enemyWaves[3] =	[	2, 	2, 	2, 	2, 	2,
								3, 	3, 	3, 	3, 	3,
								4,	4,	4,	4,	4						];	//5x (#2), 5x (#3), 5x (#4)
								
			enemyWaves[4] =	[	3, 	3, 	3, 	3, 	3,
								4,	4,	4,	4,	4, 
								5, 	5, 	5, 	5, 	5,	5, 	5, 	5, 	5, 	5	];	//5x (#3), 5x (#4), 10x (#5)
								
			enemyWaves[5] =	[	5, 	5, 	5, 	5, 	5, 	5, 	5, 	5, 	5, 	5,
								6, 	6, 	6, 	6, 	6,	6, 	6, 	6, 	6, 	6,
								6, 	6, 	6, 	6, 	6						];	//10x (#5), 15x (#6)
								
			enemyWaves[6] =	[	3, 	3, 	3, 	3, 	3, 	3, 	3, 	3, 	3, 	3,
								5, 	5, 	5, 	5, 	5, 	5, 	5, 	5, 	5, 	5,
								7, 	7, 	7, 	7, 	7,	7, 	7, 	7, 	7, 	7	];	//10x (#3), 10x (#5), 10x (#7)
								
			enemyWaves[7] =	[	5, 	5, 	5, 	5, 	5, 	5, 	5, 	5, 	5, 	5,
								6, 	6, 	6, 	6, 	6, 	6, 	6, 	6, 	6, 	6,
								8, 	8, 	8, 	8, 	8,	8, 	8, 	8, 	8, 	8,
								8, 	8, 	8, 	8, 	8,						];	//10x (#5), 10x (#6), 15x (#8)
								
			enemyWaves[8] =	[	1, 	1, 	1, 	1, 	1,	1, 	1, 	1, 	1, 	1,
								1, 	1, 	1, 	1, 	1,	1, 	1, 	1, 	1, 	1,
								1, 	1, 	1, 	1, 	1,
								4, 	4, 	4, 	4, 	4, 	4, 	4, 	4, 	4, 	4,
								7, 	7, 	7, 	7, 	7, 	7, 	7, 	7, 	7, 	7,
								9, 	9, 	9, 	9, 	9						];	//25x (#1), 10x (#4), 10x (#7), 5x (#9)
								
			enemyWaves[9] =	[	7, 	7, 	7, 	7, 	7,	7, 	7, 	7, 	7, 	7,
								7, 	7, 	7, 	7, 	7,
								8, 	8, 	8, 	8, 	8, 	8, 	8, 	8, 	8, 	8,
								9, 	9, 	9, 	9, 	9, 	9, 	9, 	9, 	9, 	9, 
								10, 10, 10, 10, 10,	10, 10, 10, 10, 10	];	//15x (#7), 10x (#8), 10x (#9), 10x (#10)
		}
		
		//Perform game over routines
		public function gameOver() {
			setGameState(GAME_OVER);
			var _gameOverDialog:GameOverDialog = new GameOverDialog(this, this, BOTTOM_MIDDLE, 0);
		}
		
		//Calculate and return the distance between two objects
		public function getDistanceToPoint(x1:int, y1:int, x2:int, y2:int):int {
			return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
		}
		
		//Handle key press
		private function HandleKeyDown(event:KeyboardEvent) {
			//Character movement
			if 		(event.keyCode == 87) north = true;	//W
			else if (event.keyCode == 65) west = true;	//A
			else if (event.keyCode == 83) south = true;	//S
			else if (event.keyCode == 68) east = true;	//D
			
			
		}
		
		//Handle key release
		private function HandleKeyUp(event:KeyboardEvent) {
			//Character movement
			if 		(event.keyCode == 87) north = false;				//W
			else if (event.keyCode == 65) west = false;					//A
			else if (event.keyCode == 83) south = false;				//S
			else if (event.keyCode == 68) east = false;					//D
			
			//Character swap
			else if (event.keyCode == 82) SwapCharacters();				//R
			
			//Switch to next weapon
			else if (event.keyCode == 69) pc.equipNextWeapon();		//E
			
			//Switch to previous weapon
			else if (event.keyCode == 81) pc.equipPrevWeapon();		//Q
			
			//Open pause menu
			//else if (event.keyCode == 80) {								//P
				//setGameState(GAME_PAUSED);
				//var _pauseDialog:PauseDialog = new PauseDialog(this, this, BOTTOM_MIDDLE, 0);
			//}
			//Open inventory
			else if (event.keyCode == 32) {				//spacebar
				setGameState(GAME_PAUSED);
				var _inventoryDialog:InventoryDialog = new InventoryDialog(this, this, MIDDLE_RIGHT, 0);
			}
		}
		
		//Complete the current wave
		public function HandleWaveComplete() {
			//Award bonus nightmare points if no damage was taken during the wave
			if (!pcDamagedThisWave) np += npThisWave * NP_MULT_FLAWLESS_WAVE;	
			
			//Unlock next wave if there is one, and if it's not already unlocked
			if (unlockedWaves[campaign - 1].indexOf(currentWaveIndex + 1) == -1) {
				if (currentWaveIndex == 9) unlockedWaves[campaign].push(0);		//if at the end of this campaign, unlock first wave of next campaign
				else unlockedWaves[campaign - 1].push(currentWaveIndex + 1);	//otherwise, unlock next wave in this campaign
			}
			
			//Remove Dream Gate if it exists
			if (dreamGate) { dreamGate.parent.removeChild(dreamGate); dreamGate = null; };
			
			setGameState(GAME_PAUSED);
			var _waveCompleteDialog:WaveCompleteDialog = new WaveCompleteDialog(this, this, MIDDLE_LEFT, 0);
		}
		
		//Hide crosshair
		public function hideCrossHair() {
			if (crossHair) {
				removeChild(crossHair);
				crossHair = null;
				Mouse.show();
			}
		}
		
		//Load slot
		public function loadGame(event:MouseEvent = null) {
			trace("Loading slot...");
		}
		
		//Add/remove health to/from a character
		public function modifyUnitHealth(unit:MovieClip, amt:int, restoreEnergy:Boolean = false, attacker:MovieClip = null) {
			//Restore energy if needed
			if (restoreEnergy && amt < 0) modifyUnitEnergy(unit, Math.ceil(-amt * ENERGY_ABSORB_PERCENT));
			
			//Then mod the health
			unit.healthCur += amt;
			
			//Cap/floor
			if (unit.healthCur > unit.healthMax) unit.healthCur = unit.healthMax;
			else if (unit.healthCur < 0) unit.healthCur = 0;
			
			//Update bar
			unit.healthBar.scaleX = unit.healthCur / unit.healthMax;
			
			//Check unit death
			if (unit.healthCur == 0) unit.killMe(attacker);
		}
		
		//Add/remove energy to/from a character
		public function modifyUnitEnergy(unit:MovieClip, amt:int) {
			//Mod energy
			unit.energyCur += amt;
			
			//Cap/floor
			if (unit.energyCur > unit.energyMax) unit.energyCur = unit.energyMax;
			else if (unit.energyCur < 0) unit.energyCur = 0;
			
			//Update bar
			unit.energyBar.scaleX = unit.energyCur / unit.energyMax;
		}
		
		//Move all onscreen projectiles based on their velocities
		private function PulseBullets() {
			//PC bullets
			for (var i:int = pcBullets.length - 1; i >= 0; i--) {
				pcBullets[i].pulse();
			}
			
			//Enemy bullets
			for (i = enemyBullets.length - 1; i >= 0; i--) {
				enemyBullets[i].pulse();
			}
		}
		
		//Move all enemies based on their AI settings and their velocities
		private function PulseEnemies() {
			for (var i:int = 0; i < enemies.length; i++) {
				enemies[i].pulse();
			}
		}
		
		//Remove everything from the screen and go to one of the game screens
		public function quitGame() {
			setGameState(GAME_OVER);
			RemoveDynamicGameChildren();
			npMultKillEnemy = .95;			//reset np multiplier
			pc.deactivateSkills(true);
			globalRecastTimerCur = 0;
		}
		
		//Quit the current wave
		public function quitWave() {
			setGameState(GAME_OVER);
			RemoveDynamicGameChildren();
			pc.deactivateSkills(false);
		}
		
		//Remove all dynamically-added onscreen game elements, excluding screens and dialogs
		private function RemoveDynamicGameChildren() {
			//PC Bullets
			for (var i:int = pcBullets.length - 1; i >= 0; i--) { pcBullets[i].killMe(); }
			
			//Enemy Bullets
			for (i = enemyBullets.length - 1; i >= 0; i--) { enemyBullets[i].killMe(); }
			
			//Enemies
			for (i = enemies.length - 1; i >= 0; i--) { enemies[i].killMe(); }
			
			//Dream Gate
			if (dreamGate) { dreamGate.parent.removeChild(dreamGate); dreamGate = null; }
			
			//Health drops
			for (i = level.numChildren - 1; i >= 0; i--) {
				if (level.getChildAt(i) is Item) Item(level.getChildAt(i)).killMe();
			}
		}
		
		//Remove all event listeners that are required for gameplay
		private function RemoveGameEventListeners() {
			removeEventListener(Event.ENTER_FRAME, GameLoop);
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);
			stage.removeEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, function() { pc.shooting = true; });
			stage.removeEventListener(MouseEvent.MOUSE_UP, function() { pc.shooting = false; });
		}
		
		//Get rid of all game screens so the play area will be visible
		public function removeGameScreens() {
			for (var i:int = numChildren - 1; i >= 0; i--) {
				if (getChildAt(i) is BaseGameScreen) MovieClip(getChildAt(i)).killMe();
			}
			
			if (screenBlocker) screenBlocker.visible = false;
		}
		
		//Save slot
		public function saveGame(event:MouseEvent = null) {
			trace("Saving slot...");
		}
		
		//Set game state and perform certain actions based on new state
		public function setGameState(newState:int) {
			gameState = newState;
			
			if 		(gameState == GAME_PLAYING) {
				showCrossHair();
				AddGameEventListeners();
			}
			else if (gameState == GAME_PAUSED) {
				hideCrossHair();
				RemoveGameEventListeners();
				StopAnimations();
			}
			else if (gameState == GAME_OVER) {
				hideCrossHair();
				RemoveGameEventListeners();
				StopAnimations();
			}
		}
		
		//Slightly shake the screen (usually when an enemy dies)
		public function shakeScreen() {
			Tweener.addTween(level, { y:level.y - 10, time:.1, transition:"easeOutBounce", onComplete:function() { level.y += 10;} } );
			//Tweener.addTween(level, { y:level.y + 25, time:.1, delay:.1, transition:"easeOutBounce" } );
		}
		
		//Show crosshair
		public function showCrossHair() {
			hideCrossHair();
			
			crossHair = new CrossHair();
			addChild(crossHair);
			Mouse.hide();
		}
		
		//Force PCs and enemies to be in front of or behind one another based on their y values
		private function SortAllByDepth() {
			for (var i:int = 0; i < level.numChildren - 1; i++) {
				for (var j:int = i + 1; j < level.numChildren; j++) {
					if ((level.getChildAt(i) is PC || level.getChildAt(i) is Enemy) && (level.getChildAt(j) is PC || level.getChildAt(j) is Enemy) && level.getChildAt(i).y > level.getChildAt(j).y) {
						level.swapChildrenAt(i, j);
					}
				}
			}
		}
		
		//Continually spawn enemies until all of them have been spawned for this wave
		private function SpawnEnemyWave() {
			//Only continue if a skill effect is not preventing enemies from spawning
			if (!pauseEnemySpawns) {
				spawnDelayTimerCur--;
				
				if (spawnDelayTimerCur <= 0 && enemies.length < MAX_ONSCREEN_ENEMIES) {
					spawnDelayTimerCur = spawnDelayTimerMax;
					
					var _randomEnemy:int = Math.random() * currentWave.length;
					var _newEnemy:Enemy = new Enemy(this, level, currentWave[_randomEnemy] + (campaign - 1) * 10);	//spawn a random enemy from this wave
					currentWave.splice(_randomEnemy, 1);								//remove that enemy from the list of enemies to spawn in this wave
				}
			}
		}
		
		//Continually spawn health drops at random locations in the level
		private function SpawnHealthDrop() {
			healthDropDelayTimerCur--;
			
			if (healthDropDelayTimerCur == 0) {
				healthDropDelayTimerCur = healthDropDelayTimerMax;
				
				//Choose a random type of health drop, modified by probability
				var _randDrop:Number = Math.random();
				
				if		(_randDrop >= .9) {
					var _dropType:String = "health50";
				}
				else if (_randDrop >= .75) {
					_dropType = "health40";
				}
				else if (_randDrop >= .5) {
					_dropType = "health25";
				}
				else _dropType = "health10";
				
				var _healthDrop:Item = new Item(_dropType);
				
				//Determine spawn zone
				var _randZone:int = Math.random() * 4;
				var _randHorizontal:int = Math.random() * RIGHT_EDGE;
				var _randVertical:int = Math.random() * BOTTOM_EDGE;
				var _leftEdge:int = pc.x - RIGHT_EDGE / 2;
				var _rightEdge:int = -_leftEdge;
				var _topEdge:int = pc.y - BOTTOM_EDGE / 2;
				var _bottomEdge:int = -_topEdge;
				
				if 		(_randZone == 0) { _healthDrop.x = Math.random() * level.wall3.x - 50; 	_healthDrop.y = Math.random() * level.wall2.y + 50; }
				else if (_randZone == 1) { _healthDrop.x = Math.random() * level.wall3.x - 50; 	_healthDrop.y = Math.random() * level.wall4.y - 50; }
				else if (_randZone == 2) { _healthDrop.x = Math.random() * level.wall1.x + 50; 	_healthDrop.y = Math.random() * level.wall2.y + 50; }
				else if (_randZone == 3) { _healthDrop.x = Math.random() * level.wall1.x + 50; 	_healthDrop.y = Math.random() * level.wall4.y - 50; }
				
				level.addChild(_healthDrop);
			}
		}
		
		//Prevent all onscreen characters from continuing to animate
		private function StopAnimations() {
			for (var i:int = 0; i < enemies.length; i++) {
				if (enemies[i].torso) 	enemies[i].torso.stop();
				if (enemies[i].legs)	enemies[i].legs.stop();
			}
			
			if (pc.torso)						pc.torso.stop();
			if (pc.torso.weaponGraphic) 		pc.torso.weaponGraphic.stop();
			if (pc.legs)						pc.legs.stop();
			
			if (partner.torso)					partner.torso.stop();
			if (partner.torso.weaponGraphic) 	partner.torso.weaponGraphic.stop();
			if (partner.legs)					partner.legs.stop();
		}
		
		//Swap characters
		private function SwapCharacters() {
			//Only swap if partner is alive
			if (!partner.dead) {
				var _tempPC:PC = partner;
				var _tempX:int = partner.x;
				var _tempY:int = partner.y;
				
				partner.x = pc.x;
				partner.y = pc.y;
				partner = pc;
				
				pc.x = _tempX;
				pc.y = _tempY;
				pc = _tempPC;
			}
		}
		
		//Update crosshair's position
		private function UpdateCrossHair() {
			if (crossHair) {
				crossHair.x = mouseX;
				crossHair.y = mouseY;
			}
		}
	}
}
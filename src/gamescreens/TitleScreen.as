﻿package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class TitleScreen extends BaseGameScreen {
		//METHODS / Constructor
		public function TitleScreen(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {
			btnNewGame.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(NewGameScreen, gameRoot.TOP_MIDDLE); killMe(gameRoot.BOTTOM_MIDDLE); } );
			//btnLoadGame.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(LoadGameScreen, gameRoot.BOTTOM_MIDDLE); killMe(gameRoot.TOP_MIDDLE); } );
			btnCredits.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(CreditsScreen, gameRoot.MIDDLE_LEFT); killMe(gameRoot.MIDDLE_RIGHT); } );
			btnHelp.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(HelpScreen, gameRoot.MIDDLE_RIGHT); killMe(gameRoot.MIDDLE_LEFT); } );
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			btnNewGame.removeEventListener(MouseEvent.CLICK, function() { ShowGameScreen(NewGameScreen, gameRoot.TOP_MIDDLE); killMe(gameRoot.BOTTOM_MIDDLE); } );
			//btnLoadGame.removeEventListener(MouseEvent.CLICK, function() { ShowGameScreen(LoadGameScreen, gameRoot.BOTTOM_MIDDLE); killMe(gameRoot.TOP_MIDDLE); } );
			btnCredits.removeEventListener(MouseEvent.CLICK, function() { ShowGameScreen(CreditsScreen, gameRoot.MIDDLE_LEFT); killMe(gameRoot.MIDDLE_RIGHT); } );
			btnHelp.removeEventListener(MouseEvent.CLICK, function() { ShowGameScreen(HelpScreen, gameRoot.MIDDLE_RIGHT); killMe(gameRoot.MIDDLE_LEFT); } );
		}
	}
}
package gamescreens {
	//IMPORTS
	import chars.PC;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class InventoryDialog extends BaseGameScreen {
		//METHODS / Constructor
		public function InventoryDialog(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
			
			FillInventorySlots(Item.INVENTORY_GROUP_WEAPONS_MINDY, gameRoot.mindy.invWeapons);
			FillInventorySlots(Item.INVENTORY_GROUP_WEAPONS_BINKY, gameRoot.binky.invWeapons);
			FillInventorySlots(Item.INVENTORY_GROUP_SKILLS_MINDY, gameRoot.mindy.invSkills);
			FillInventorySlots(Item.INVENTORY_GROUP_SKILLS_BINKY, gameRoot.binky.invSkills);
			
			UpdateIconCooldownBoxes();
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {				
			gameRoot.stage.addEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
			btnClose.addEventListener(MouseEvent.CLICK, function() { RemoveInventorySlots(); killMe(gameRoot.MIDDLE_RIGHT, 0); } );
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			gameRoot.stage.removeEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
			btnClose.removeEventListener(MouseEvent.CLICK, function() { RemoveInventorySlots(); killMe(gameRoot.MIDDLE_RIGHT, 0); } );
		}
		
		
		//Show all currently held weapons and skills in the available slots and allow them to be clicked
		private function FillInventorySlots(slotGroup:String, itemList:Array) {
			//Display those items and allow them to be clicked
			for (var i:int = 1; i < 7; i++) {
				var thisSlot:Item = this[slotGroup + i];
				thisSlot.inventoryGroup = slotGroup;
				
				//If there are still items left in the received list, keep filling up inventory slots and activating the item
				if (itemList[i - 1]) {
					thisSlot.itemName = itemList[i - 1];
					thisSlot.gotoAndStop(thisSlot.itemName);
					thisSlot.activateButtonMode();
				}
				//Otherwise, clear the current slot
				else {
					thisSlot.itemName = null;
					thisSlot.gotoAndStop(1);
					thisSlot.deactivateButtonMode();
				}
			}
		}
		
		//Kill self when spacebar is released
		private function HandleKeyUp(event:KeyboardEvent) {
			if (event.keyCode == 32) {
				RemoveInventorySlots(); 
				killMe(gameRoot.MIDDLE_RIGHT, 0);
			}
		}
		
		//Remove all inventory slots and their event listeners
		private function RemoveInventorySlots() {
			for (var i:int = numChildren - 1; i >= 0; i--) {
				if (getChildAt(i) is Item) Item(getChildAt(i)).killMe();
			}
		}
		
		//Fill a gray box above the item icons to indicate their current cooldown amount
		public function UpdateIconCooldownBoxes() {
			for (var i:int = 1; i < 13; i++) {
				if (i < 7) {
					var _thisSlot:Item = this[Item.INVENTORY_GROUP_SKILLS_MINDY + i];
					var _skillOwner:PC = gameRoot.mindy;
				}
				else {
					_thisSlot = this[Item.INVENTORY_GROUP_SKILLS_BINKY + (i - 6)];
					_skillOwner = gameRoot.binky;
				}
				
				if (_thisSlot.currentFrame > 1) {
					var _globalPercent:Number = gameRoot.globalRecastTimerCur / gameRoot.GLOBAL_RECAST_TIMER_MAX;
					//var _skillPercent:Number = _skillOwner.invSkills[_thisSlot.currentLabel].recastTimerCur / _skillOwner.invSkills[_thisSlot.currentLabel].recastTimerMax;
					
					//if (_globalPercent >= _skillPercent) 
					_thisSlot.grayOut.visible = true;
					_thisSlot.grayOut.scaleY = _globalPercent;
					//else _thisSlot.grayOut.scaleY = _skillPercent;
				}
				else _thisSlot.grayOut.visible = false;
			}
		}
	}
}
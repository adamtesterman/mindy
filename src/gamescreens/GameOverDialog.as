﻿package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class GameOverDialog extends BaseGameScreen {
		//METHODS / Constructor
		public function GameOverDialog(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {			
			btnWaveSelect.addEventListener(MouseEvent.CLICK, QuitToWaveSelect);
			btnMain.addEventListener(MouseEvent.CLICK, QuitToTitle);
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			btnWaveSelect.removeEventListener(MouseEvent.CLICK, QuitToWaveSelect);
			btnMain.removeEventListener(MouseEvent.CLICK, QuitToTitle);
		}
	}
}
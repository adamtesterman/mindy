package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class NewGameScreen extends BaseGameScreen {
		//METHODS / Constructor
		public function NewGameScreen(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {
			btnAdventure.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(WaveSelectScreen, gameRoot.TOP_MIDDLE); killMe(gameRoot.BOTTOM_MIDDLE); } );
			//btnSurvival.addEventListener(MouseEvent.CLICK, function() {} );
			btnMain.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(TitleScreen, gameRoot.BOTTOM_MIDDLE); killMe(gameRoot.TOP_MIDDLE); } );
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			btnAdventure.removeEventListener(MouseEvent.CLICK, function() { ShowGameScreen(WaveSelectScreen, gameRoot.TOP_MIDDLE); killMe(gameRoot.BOTTOM_MIDDLE); } );
			//btnSurvival.removeEventListener(MouseEvent.CLICK, function() {} );
			btnMain.removeEventListener(MouseEvent.CLICK, function() { ShowGameScreen(TitleScreen, gameRoot.BOTTOM_MIDDLE); killMe(gameRoot.TOP_MIDDLE); } );
		}	
	}
}
﻿package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import utils.SoundController;
	
	import utils.caurina.transitions.Tweener;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class BaseGameScreen extends MovieClip {
		//PROPERTIES / Constants
		private const TWEEN_TRANSITION_TYPE_MOVEMENT:String =		"easeOutExpo";
		private const TWEEN_TRANSITION_TYPE_ALPHA:String =			"linear";
		private const TWEEN_TRANSITION_TIME_MOVEMENT:Number =		1;					//seconds
		private const TWEEN_TRANSITION_TIME_ALPHA:Number =			.5;
		
		//PROPERTIES / Objects
		protected var gameRoot:MovieClip;		//reference to Main
		protected var myParent:MovieClip;		//parent object (usually Main)
		protected var me:BaseGameScreen;		//required for anonymous function calls that pass "this"
		
		
		//METHODS / Constructor
		public function BaseGameScreen(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			//New properties
			gameRoot = rootRef;
			myParent = displayParent;
			me = this;
			gameRoot.numGameScreens++;
			
			//New methods
			gameRoot.hideCrossHair();
			AddEventListeners();
			Tweener.addTween(this, { x:gameRoot.MIDDLE_MIDDLE.x, y:gameRoot.MIDDLE_MIDDLE.y, time:TWEEN_TRANSITION_TIME_MOVEMENT, transition:TWEEN_TRANSITION_TYPE_MOVEMENT } );	//tween in
			Tweener.addTween(this, { alpha:1, time:TWEEN_TRANSITION_TIME_ALPHA, transition:TWEEN_TRANSITION_TYPE_ALPHA } );
			
			//Inherited properties
			x = tweenFromPoint.x;
			y = tweenFromPoint.y;
			alpha = tweenFromAlpha;
			
			//Inherited methods
			myParent.addChild(this);
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected function AddEventListeners() {
			
		}
		
		
		//OVERRIDE METHODS / Destroy Self
		public function killMe(tweenToPoint:Point, tweenToAlpha:int = 1) {
			RemoveEventListeners();
			Tweener.addTween(this, { x:tweenToPoint.x, y:tweenToPoint.y, time:TWEEN_TRANSITION_TIME_MOVEMENT, transition:TWEEN_TRANSITION_TYPE_MOVEMENT, onComplete:function() { myParent.removeChild(this); } } );
			Tweener.addTween(this, { alpha:tweenToAlpha, time:TWEEN_TRANSITION_TIME_ALPHA, transition:TWEEN_TRANSITION_TYPE_ALPHA } );
			
			gameRoot.numGameScreens--;
			if (gameRoot.numGameScreens == 0 && gameRoot.gameState == gameRoot.GAME_PAUSED) gameRoot.setGameState(gameRoot.GAME_PLAYING);	//check unpause game
			gameRoot.stage.focus = gameRoot.stage;
			gameRoot.north = gameRoot.south = gameRoot.east = gameRoot.west = false;		//reset to prevent a bug
			if (gameRoot.pc) gameRoot.pc.shooting = false;
			gameRoot.tooltip.visible = false;
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected function RemoveEventListeners() {
			
		}
		
		//Show new game screen
		protected function ShowGameScreen(screen:Class, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			var _gameScreen:BaseGameScreen = new screen(gameRoot, myParent, tweenFromPoint, tweenFromAlpha);
		}
		
		
		//Quit game and show wave select screen
		protected function QuitToWaveSelect(event:MouseEvent = null) {
			gameRoot.quitWave();
			ShowGameScreen(WaveSelectScreen, gameRoot.BOTTOM_MIDDLE);
			SoundController.playMusic(gameRoot.bgmTitle);
			killMe(gameRoot.TOP_MIDDLE, 0);
		}
		
		//Quit game and show title screen
		protected function QuitToTitle(event:MouseEvent = null) {
			gameRoot.quitGame();
			ShowGameScreen(TitleScreen, gameRoot.BOTTOM_MIDDLE);
			SoundController.playMusic(gameRoot.bgmTitle);
			killMe(gameRoot.TOP_MIDDLE, 0);
		}
	}
}
package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class CreditsScreen extends BaseGameScreen {
		//METHODS / Constructor
		public function CreditsScreen(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {
			btnMain.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(TitleScreen, gameRoot.MIDDLE_RIGHT); killMe(gameRoot.MIDDLE_LEFT); } );
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			btnMain.removeEventListener(MouseEvent.CLICK, function() { ShowGameScreen(TitleScreen, gameRoot.MIDDLE_RIGHT); killMe(gameRoot.MIDDLE_LEFT); } );
		}
	}
}
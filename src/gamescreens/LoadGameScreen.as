package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class LoadGameScreen extends BaseGameScreen {
		//METHODS / Constructor
		public function LoadGameScreen(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {
			btnLoadGame1.addEventListener(MouseEvent.CLICK, gameRoot.loadGame);
			btnLoadGame2.addEventListener(MouseEvent.CLICK, gameRoot.loadGame);
			btnLoadGame3.addEventListener(MouseEvent.CLICK, gameRoot.loadGame);
			btnMain.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(TitleScreen, gameRoot.TOP_MIDDLE); killMe(gameRoot.BOTTOM_MIDDLE); } );
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			btnLoadGame1.removeEventListener(MouseEvent.CLICK, gameRoot.loadGame);
			btnLoadGame2.removeEventListener(MouseEvent.CLICK, gameRoot.loadGame);
			btnLoadGame3.removeEventListener(MouseEvent.CLICK, gameRoot.loadGame);
			btnMain.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(TitleScreen, gameRoot.TOP_MIDDLE); killMe(gameRoot.BOTTOM_MIDDLE); } );
		}	
	}
}
﻿package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	import flash.text.*;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class WaveCompleteDialog extends BaseGameScreen {
		//METHODS / Constructor
		public function WaveCompleteDialog(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
			
			txtNPThisWave.text = gameRoot.npThisWave.toString();
			txtTotalNP.text = gameRoot.np.toString();
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {			
			btnNextWave.addEventListener(MouseEvent.CLICK, function() { gameRoot.quitWave(); gameRoot.startWave(gameRoot.currentWaveIndex + 1, false); killMe(gameRoot.MIDDLE_RIGHT, 0); } );
			btnWaveSelect.addEventListener(MouseEvent.CLICK, QuitToWaveSelect);
			btnMain.addEventListener(MouseEvent.CLICK, QuitToTitle);
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			btnNextWave.removeEventListener(MouseEvent.CLICK, function() { gameRoot.quitWave(); gameRoot.startWave(gameRoot.currentWaveIndex + 1, false); killMe(gameRoot.MIDDLE_RIGHT, 0); } );
			btnWaveSelect.removeEventListener(MouseEvent.CLICK, QuitToWaveSelect);
			btnMain.removeEventListener(MouseEvent.CLICK, QuitToTitle);
		}
	}
}
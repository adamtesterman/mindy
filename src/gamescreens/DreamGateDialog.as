﻿package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	import flash.text.*;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class DreamGateDialog extends BaseGameScreen {
		//METHODS / Constructor
		public function DreamGateDialog(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
			
			//Inherited properties
			txtNPOwned.text = gameRoot.np;
			txtNPRequired.text = gameRoot.campaignNPRequirements[gameRoot.campaign];
			
			//Inherited methods
			
			if (int(txtNPOwned.text) < int(txtNPRequired.text) || gameRoot.campaign == 2) removeChild(btnContinue);
			else gameRoot.np -= int(txtNPRequired.text);
			gameRoot.dreamGate.parent.removeChild(gameRoot.dreamGate);
			gameRoot.dreamGate = null;
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {		
			if (this.contains(btnContinue)) btnContinue.addEventListener(MouseEvent.CLICK, function() { killMe(gameRoot.MIDDLE_RIGHT, 0); gameRoot.HandleWaveComplete(); } );
			btnWaveSelect.addEventListener(MouseEvent.CLICK, QuitToWaveSelect);
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			if (this.contains(btnContinue)) btnContinue.removeEventListener(MouseEvent.CLICK, function() { killMe(gameRoot.MIDDLE_RIGHT, 0); gameRoot.HandleWaveComplete(); } );
			btnWaveSelect.removeEventListener(MouseEvent.CLICK, QuitToWaveSelect);
		}
	}
}
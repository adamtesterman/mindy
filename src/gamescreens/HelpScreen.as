package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class HelpScreen extends BaseGameScreen {
		//METHODS / Constructor
		public function HelpScreen(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
			
			//Inherited methods
			btnPrev.visible = false;	//can't see prev button on first frame
			helpContent.stop();
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {
			//Add event listeners and tween in (in Base)
			btnPrev.addEventListener(MouseEvent.CLICK, PrevFrame);
			btnNext.addEventListener(MouseEvent.CLICK, NextFrame);
			btnMain.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(TitleScreen, gameRoot.MIDDLE_LEFT); killMe(gameRoot.MIDDLE_RIGHT); } );
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			btnPrev.removeEventListener(MouseEvent.CLICK, PrevFrame);
			btnNext.removeEventListener(MouseEvent.CLICK, NextFrame);
			btnMain.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(TitleScreen, gameRoot.MIDDLE_LEFT); killMe(gameRoot.MIDDLE_RIGHT); } );
		}
		
		
		//Go to next frame in help menu
		private function NextFrame(event:MouseEvent = null) {
			helpContent.nextFrame();
			btnPrev.visible = true;
			
			if (helpContent.currentFrame == helpContent.totalFrames) btnNext.visible = false;
			else btnNext.visible = true;
		}
		
		//Go to previous frame in help menu
		private function PrevFrame(event:MouseEvent = null) {
			helpContent.prevFrame();
			btnNext.visible = true;
			
			if (helpContent.currentFrame == 1) btnPrev.visible = false;
			else btnPrev.visible = true;
		}
	}
}
﻿package gamescreens {
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	
	import utils.SoundController;
	
	public class StoryDialog extends MovieClip {
		//PROPERTIES / Objects
		public var gameRoot:MovieClip;		//reference to main
		
		
		//METHODS / Constructor
		public function StoryDialog() {
			//New methods
			AddEventListeners();
			
			
			//Inherited properties
			x = 320;
			y = 240;
			
			//Inherited methods
			addEventListener(Event.ADDED_TO_STAGE, HandleAddedToStage);
		}
		
		
		//METHODS / Added as Child
		private function HandleAddedToStage(event:Event) {
			removeEventListener(Event.ADDED_TO_STAGE, HandleAddedToStage);
			
			SoundController.stopMusic();
			SoundController.playVoice(gameRoot.voiStory01);
		}
		
		
		//METHODS / Deconstructor
		public function killMe(event:MouseEvent = null) {
			gameRoot.setGameState(gameRoot.GAME_PLAYING);						//add appropriate game event listeners
			SoundController.playMusic(gameRoot["bgmLevel0" + gameRoot.campaign]);
			SoundController.stopVoice();
				
			stage.focus = stage;
			
			RemoveEventListeners();
			parent.removeChild(this);
		}
		
		
		//METHODS / Add Event Listeners
		private function AddEventListeners() {
			addEventListener(Event.ENTER_FRAME, ScrollText);
			btnSkip.addEventListener(MouseEvent.CLICK, killMe);
		}
		
		
		//METHODS / Remove Event Listeners
		private function RemoveEventListeners() {
			removeEventListener(Event.ENTER_FRAME, ScrollText);
			btnSkip.removeEventListener(MouseEvent.CLICK, killMe);
		}
		
		
		//Scroll text upward
		private function ScrollText(event:Event) {
			if (txtStoryText.y > -635) txtStoryText.y -= .5;
		}
	}
}
package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class PauseDialog extends BaseGameScreen {
		//METHODS / Constructor
		public function PauseDialog(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {		
			//btnSaveLoadGame.addEventListener(MouseEvent.CLICK, function() { ShowGameScreen(SaveLoadDialog, gameRoot.BOTTOM_MIDDLE, 0); } );
			btnWaveSelect.addEventListener(MouseEvent.CLICK, QuitToWaveSelect);
			btnMain.addEventListener(MouseEvent.CLICK, QuitToTitle);
			btnClose.addEventListener(MouseEvent.CLICK, function() { killMe(gameRoot.BOTTOM_MIDDLE, 0); } );
			gameRoot.stage.addEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			//btnSaveLoadGame.removeEventListener(MouseEvent.CLICK, function() { ShowGameScreen(SaveLoadDialog, gameRoot.BOTTOM_MIDDLE, 0); } );
			btnWaveSelect.removeEventListener(MouseEvent.CLICK, QuitToWaveSelect);
			btnMain.removeEventListener(MouseEvent.CLICK, QuitToTitle);
			btnClose.removeEventListener(MouseEvent.CLICK, function() { killMe(gameRoot.BOTTOM_MIDDLE, 0); } );
			gameRoot.stage.removeEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
		}
		
		
		//Kill self when "P" is released
		private function HandleKeyUp(event:KeyboardEvent) {
			if (event.keyCode == 80) killMe(gameRoot.BOTTOM_MIDDLE, 0);
		}
	}
}
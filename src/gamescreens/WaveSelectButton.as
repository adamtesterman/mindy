package gamescreens {
	//IMPORTS
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextField
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class WaveSelectButton extends MovieClip {
		//PROPERTIES / Constants
		private var SHADOW_BLUR_X:int =		5;
		private var SHADOW_BLUR_Y:int =		SHADOW_BLUR_X;
		private var SHADOW_COLOR:Number =	0x000000;
		
		//PROPERTIES / Variables
		public var buttonValue:int;			//the wave number on this button
		public var isLocked:Boolean = true;
		private var myStartX:int;
		private var myStartY:int;
		
		//PROPERTIES / Objects
		private var gameRoot:MovieClip;		//reference to Main
		private var myParent:MovieClip;		//parent object
		
		
		
		//METHODS / Constructor
		public function WaveSelectButton(rootRef:MovieClip, displayParent:MovieClip, startX:int, startY:int, buttonText:String) {
			//New properties
			gameRoot = rootRef;
			myParent = displayParent;
			buttonValue = int(buttonText);
			var dropShadow:DropShadowFilter = new DropShadowFilter;
			dropShadow.color = SHADOW_COLOR;
			dropShadow.blurX = dropShadow.blurY = SHADOW_BLUR_X;
			filters = [dropShadow];
			
			//New methods
			updateLockStatus();
			
			//Inherited properties
			x = myStartX = startX;
			y = myStartY = startY;
			txtWave.mouseEnabled = false;
			txtWave.text = buttonText;
			
			//Inherited methods
			gotoAndStop(gameRoot.campaign);
			myParent.addChild(this);
		}
		
		
		//Add button state event listeners
		public function addEventListeners() {
			addEventListener(MouseEvent.MOUSE_DOWN, HandleMouseDown);
			addEventListener(MouseEvent.MOUSE_OUT, HandleMouseOut);
			addEventListener(MouseEvent.MOUSE_OVER, HandleMouseOver);
			addEventListener(MouseEvent.MOUSE_UP, HandleMouseUp);
		}
		
		
		//Upon mouse down
		private function HandleMouseDown(event:MouseEvent) {
			x += 5;
			y += 5;
		}
		
		//Upon mouse out
		private function HandleMouseOut(event:MouseEvent) {
			txtWave.filters = [txtWave.filters[0]];
			
			x = myStartX;
			y = myStartY;
		}
		
		//Upon mouse over
		private function HandleMouseOver(event:MouseEvent) {
			var _textGlowFilter:GlowFilter = new GlowFilter;
			_textGlowFilter.color = 0xFF9900;
			_textGlowFilter.blurX = _textGlowFilter.blurY = 14;
			_textGlowFilter.strength = 1.4;
			
			txtWave.filters = [txtWave.filters[0], _textGlowFilter];
		}
		
		//Upon mouse up
		private function HandleMouseUp(event:MouseEvent) {
			x = myStartX;
			y = myStartY;
			
			myParent.killMe(gameRoot.BOTTOM_MIDDLE);
			gameRoot.startWave(buttonValue - 1, true);
		}
		
		//Remove button state event listeners
		public function removeEventListeners() {
			removeEventListener(MouseEvent.MOUSE_DOWN, HandleMouseDown);
			removeEventListener(MouseEvent.MOUSE_OUT, HandleMouseOut);
			removeEventListener(MouseEvent.MOUSE_OVER, HandleMouseOver);
			removeEventListener(MouseEvent.MOUSE_UP, HandleMouseUp);
		}
		
		//Show or hide lock depending on main's list of unlocked waves per campaign
		public function updateLockStatus() {
			if (gameRoot.unlockedWaves[gameRoot.campaign - 1].indexOf(buttonValue - 1) > -1) {
				isLocked = false;
				lock.visible = false;
				addEventListeners();
				buttonMode = true;
			}
			else {
				isLocked = true;
				lock.visible = true;
				removeEventListeners();
				buttonMode = false;
			}
		}
	}
}
package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SaveLoadDialog extends BaseGameScreen {
		//METHODS / Constructor
		public function SaveLoadDialog(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {			
			btnSaveGame.addEventListener(MouseEvent.CLICK, gameRoot.saveGame);
			btnLoadGame.addEventListener(MouseEvent.CLICK, gameRoot.loadGame);
			btnClose.addEventListener(MouseEvent.CLICK, function() { killMe(gameRoot.BOTTOM_MIDDLE, 0); } );
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			btnSaveGame.removeEventListener(MouseEvent.CLICK, gameRoot.saveGame);
			btnLoadGame.removeEventListener(MouseEvent.CLICK, gameRoot.loadGame);
			btnClose.removeEventListener(MouseEvent.CLICK, function() { killMe(gameRoot.BOTTOM_MIDDLE, 0); } );
		}	
	}
}
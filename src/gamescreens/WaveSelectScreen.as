﻿package gamescreens {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	import gamescreens.*;
	
	import utils.caurina.transitions.Tweener;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class WaveSelectScreen extends BaseGameScreen {
		//PROPERTIES / Constants
		private const WAVE_BUTTON_BUFFER_WIDTH:int =	20;
		private const WAVE_BUTTON_BUFFER_HEIGHT:int =	10;
		
		private const WAVE_BUTTON_WIDTH:int =			65 + WAVE_BUTTON_BUFFER_WIDTH;
		private const WAVE_BUTTON_HEIGHT:int =			50 + WAVE_BUTTON_BUFFER_HEIGHT;
		
		private const WAVE_BUTTON_0_START_X:int =		-170;
		private const WAVE_BUTTON_0_START_Y:int =		-10;
		
		
		//METHODS / Constructor
		public function WaveSelectScreen(rootRef:MovieClip, displayParent:MovieClip, tweenFromPoint:Point, tweenFromAlpha:int = 1) {
			super(rootRef, displayParent, tweenFromPoint, tweenFromAlpha);
			
			//New properties
				//Add wave buttons
				for (var i:int = 0; i < 5; i++) {		//first row
					var _newButton:WaveSelectButton = new WaveSelectButton(gameRoot, this, WAVE_BUTTON_0_START_X + i * WAVE_BUTTON_WIDTH, WAVE_BUTTON_0_START_Y, (i + 1).toString());
				}
				for (i = 0; i < 5; i++) {				//second row
					_newButton = new WaveSelectButton(gameRoot, this, WAVE_BUTTON_0_START_X + i * WAVE_BUTTON_WIDTH, WAVE_BUTTON_0_START_Y + WAVE_BUTTON_HEIGHT + WAVE_BUTTON_BUFFER_HEIGHT, (i + 6).toString());
				}
			
			//Inherited properties
			campaignDot.x = this["btnCampaign" + (gameRoot.campaign)].x;
			campaignDot.y = this["btnCampaign" + (gameRoot.campaign)].y;
		}
		
		
		//OVERRIDE METHODS / Add Event Listeners
		protected override function AddEventListeners() {
			btnMain.addEventListener(MouseEvent.CLICK, QuitToTitle);
			btnCampaign1.addEventListener(MouseEvent.CLICK, ChangeCampaign);
			btnCampaign2.addEventListener(MouseEvent.CLICK, ChangeCampaign);
		}
		
		
		//OVERRIDE METHODS / Remove Event Listeners
		protected override function RemoveEventListeners() {
			//Remove wave button event listeners
			for (var i:int = numChildren - 1; i >= 0; i--) {
				if (getChildAt(i) is WaveSelectButton) WaveSelectButton(getChildAt(i)).removeEventListeners();
			}
			
			btnMain.removeEventListener(MouseEvent.CLICK, QuitToTitle);
			btnCampaign1.removeEventListener(MouseEvent.CLICK, ChangeCampaign);
			btnCampaign2.removeEventListener(MouseEvent.CLICK, ChangeCampaign);
		}
		
		
		//Change wave select buttons to appropriate color based on campaign
		private function ChangeCampaign(event:MouseEvent) {
			//First, update Main's campaign number based on button clicked
			if 		(event.currentTarget == btnCampaign1) gameRoot.campaign = 1;
			else if (event.currentTarget == btnCampaign2) gameRoot.campaign = 2;
			
			campaignDot.x = event.currentTarget.x;
			campaignDot.y = event.currentTarget.y;
			
			//Tween each button
			for (var i:int = 0; i < numChildren; i++) {
				if (getChildAt(i) is WaveSelectButton) ShrinkButton(WaveSelectButton(getChildAt(i)));
			}
		}
		
		//Grow the button horizontally until it reaches full scaleX
		private function GrowButton(thisButton:WaveSelectButton) {
			thisButton.gotoAndStop(gameRoot.campaign);
			thisButton.updateLockStatus();
			Tweener.addTween(thisButton, { scaleX:1, time:.25, transition:"easeInExpo" } );
		}	
		
		//Shrink the button horizontally until it reaches invisibility
		private function ShrinkButton(thisButton:WaveSelectButton) {
			thisButton.removeEventListeners();
			Tweener.addTween(thisButton, { scaleX:0, time:.25, transition:"easeOutExpo", onComplete:function () { GrowButton(thisButton); } } );
		}
	}
}
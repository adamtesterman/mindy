﻿package {
	import flash.display.*;
	import flash.text.*;
	import flash.events.*;
	
	public class Fader extends MovieClip {
		//PROPERTIES / Constants
		public static const FADER_POOF:int =			1;		//fader types, corresponds to frame labels
		public static const FADER_TEXT:int =			2;
		public static const FADER_SKULL:int =			3;
		public static const FADER_BONE:int =			4;
		public static const FADER_BLOOD:int =			5;
		
		//PROPERTIES / Variables
		private var dx:Number = 0;
		private var dy:Number = 0;
		public var dAlpha:Number = 0;
		private var dScaleX:Number = 0;
		private var dScaleY:Number = 0;
		public var dRotation:Number = 0;
		public var destinationX:int;
		public var destinationY:int;
		
		//PROPERTIES / Objects
		private var gameRoot:MovieClip;
		private var myParent:MovieClip;
		
		
		//METHODS / Constructor
		public function Fader(rootRef:MovieClip, displayParent:MovieClip, faderType:int, startX:int, startY:int, textValue:String = "") {
			//New properties
			gameRoot = rootRef;
			myParent = displayParent;
			
			//Inherited properties
			x = startX;
			y = startY;
			
			//New methods
			addEventListener(Event.ENTER_FRAME, FadeOut);
			
			//Inherited methods
			myParent.addChild(this);
			
			//Type-specific setup
			if 		(faderType == Fader.FADER_POOF) {
				alpha = Math.random() + .5;
				scaleX = Math.random() + .5;
				scaleY = scaleX;
				rotation = Math.random() * 360;
				
				dAlpha = -.03;
				dScaleX = -.05;
				dScaleY = dScaleX;
				dRotation = 1;
			}
			else if (faderType == Fader.FADER_TEXT) {
				dy = -1;
				dAlpha = -.01;
				
				addFrameScript(faderType - 1, function() { txtTextField.text = textValue; } );
			}
			
			gotoAndStop(faderType);
		}
		
		
		//METHODS / Destroy Self
		public function killMe() {
			RemoveEventListeners();
			myParent.removeChild(this);
		}
		
		
		//Fade out and disappear, then destroy self
		private function FadeOut(event:Event) {
			x += dx;
			y += dy;
			alpha += dAlpha;
			scaleX += dScaleX;
			scaleY += dScaleY;
			rotation += dRotation;

			if (alpha <= 0) killMe();
		}
		
		//Remove all event listeners
		private function RemoveEventListeners() {
			removeEventListener(Event.ENTER_FRAME, FadeOut);
		}
	}
}
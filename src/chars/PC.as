﻿package chars {
	//IMPORTS
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	
	import chars.*;
	
	public class PC extends BaseCharacter {
		//PROPERTIES / Variables
		public var shooting:Boolean;
		
		//PROPERTIES / Objects
		public var healthBar:MovieClip;
		public var energyBar:MovieClip;
		
		
		//METHODS / Constructor
		public function PC(rootRef:MovieClip, displayParent:MovieClip, characterType:int) {
			super(rootRef, displayParent, characterType);
			
			//Inherited methods
			addFrameScript(0, function() { torso.gotoAndStop(START_FACING); legs.gotoAndStop(START_FACING); });
			addFrameScript(1, function() { torso.gotoAndStop(START_FACING); legs.gotoAndStop(START_FACING); });
			
			//Type-specific setup
				//MINDY
				if 		(currentFrame == gameRoot.PC_MINDY) {
					speed = 7;
					healthMax = 100;
					healthBar = gameRoot.hud.mindyHealthBar;
					energyBar = gameRoot.hud.mindyEnergyBar;
					invWeapons.push(Item.WEAPON_GUMBALL_BLASTER);
					invSkills.push(BaseCharacter.SKILL_BRAT);
					invSkills.push(BaseCharacter.SKILL_CRYBABY);
				}
				//BINKY
				else if (currentFrame == gameRoot.PC_BINKY) {
					y = -50;
					speed = 7;
					healthMax = 100;
					healthBar = gameRoot.hud.binkyHealthBar;
					energyBar = gameRoot.hud.binkyEnergyBar;
					invWeapons.push(Item.WEAPON_BUSTERS);
					invSkills.push(BaseCharacter.SKILL_GRIZZLY);
					invSkills.push(BaseCharacter.SKILL_HYPNOSIS);
				}
				else trace("Invalid character type passed to PC constructor.");
				
				equipWeapon(invWeapons[0]);					//equip first weapon
				gameRoot.modifyUnitHealth(this, healthMax);	//fill life
				gameRoot.modifyUnitEnergy(this, energyMax);	//fill energy
		}
		
		
		//METHODS / Destroy Self
		public function killMe(killer:MovieClip = null) {
			//Remove Nightmare Skill effects
			deactivateSkills(false);
			
			//Partner death
			if (gameRoot.partner == this) {
				dead = true;
				
				torso.gotoAndStop("death");
				legs.gotoAndStop("death");
			}
			//PC death
			else gameRoot.gameOver();
		}
		
		
		//Detect collisions with walls, dream gates, and item drops
		private function DetectCollisions() {
			//Walls
			if (hitBox.hitTestObject(myParent.wall1) ||
				hitBox.hitTestObject(myParent.wall2) ||
				hitBox.hitTestObject(myParent.wall3) ||
				hitBox.hitTestObject(myParent.wall4)) {
				
				myParent.x += dx;
				myParent.y += dy;
				
				x -= dx;
				y -= dy;
			}
			
			//Item drops
			for (var i:int = 0; i < myParent.numChildren; i++) {
				if (myParent.getChildAt(i) is Item && torso.hitTestObject(myParent.getChildAt(i))) {
					var _healthDrop:Item = Item(myParent.getChildAt(i));
					var _restoreAmount:String = _healthDrop.currentLabel.replace("health", "");		//remove "health" from the label and use the numerical value that is left behind
					
					_healthDrop.killMe();
					_healthDrop = null;
					
					gameRoot.modifyUnitHealth(this, int(_restoreAmount));
					
					return;
				}
			}
			
			//Dream Gate
			if (gameRoot.dreamGate && hitTestObject(gameRoot.dreamGate)) gameRoot.checkWaveComplete();
		}
		
		//Update position based on keys pressed and velocity, perform collision detection, and shoot target if possible
		public function pulse() {
			//Only do this if alive
			if (!dead) {
				optCounter++;
				//NIGHTMARE SKILL EFFECTS (speed mod, damage mod)
				
				UpdateVelocities();
				DetectCollisions();
				shoot();
				UpdateGraphics();
			}
			
			//UpdateSkillCooldowns();
		}
		
		//Reset position, nightmare skill effects, and revive self if needed. If resetting due to starting a wave from wave select, then even more properties will be reset
		public function reset() {
			//New properties
			dead = false;
			
			//Inherited properties
			x = START_X;
			if (gameRoot.pc == this) 	y = START_Y;
			else 						y = START_Y - 50;
				
			//New methods
			gameRoot.modifyUnitHealth(this, healthMax);	//fill life
			gameRoot.modifyUnitEnergy(this, energyMax);	//fill energy
			
			//Inherited methods			
			torso.gotoAndStop(START_FACING);
			legs.gotoAndStop(START_FACING);
			
			//RESET NIGHTMARE SKILL EFFECTS
		}
		
		//Just bring the character back from the dead
		public function revive() {
			trace("reviving");
			dead = false;
			
			gameRoot.modifyUnitHealth(this, healthMax);	//fill life
		}
		
		//Set weapon stats based on weapon type
		protected override function SetWeaponParams() {
			currentWeaponIndex = invWeapons.indexOf(weapon);
			
			if		(weapon == Item.WEAPON_MAYHEM_CANNON) {
				weaponDamage = 						500 * weaponDamageBonus;	//damage per shot, modified by the damage bonus (which defaults to 1, or 100% of normal damage)
				weaponRange = 						500;		//how close the shooter (excluding currently controlled character) must be to their target (in pixels) before being able to fire this weapon
				weaponFireRateMax = 				30;			//number of frames that pass between each shot (30 FPS)
				weaponBulletSpeed =					10;			//how quickly the bullet travels across the screen (pixels per frame)
				weaponBulletRange =					-1;			//-1 is infinite (flies until it hits something or goes offscreen)
				weaponBulletPierces =				false;		//whether or not bullet keeps going after impact
			}
			else if (weapon == Item.WEAPON_FLAMETHROWER) {
				weaponDamage = 						2 * weaponDamageBonus;
				weaponRange = 						200;
				weaponFireRateMax = 				3;
				weaponBulletSpeed =					5;
				weaponBulletRange =					200;
				weaponBulletPierces =				true;
			}
			else if (weapon == Item.WEAPON_GUMBALL_BLASTER) {
				weaponDamage = 						20 * weaponDamageBonus;		//80 DPS
				weaponRange = 						500;
				weaponFireRateMax = 				7;
				weaponBulletSpeed =					10;
				weaponBulletRange =					-1;
				weaponBulletPierces =				false;
			}
			else if (weapon == Item.WEAPON_BUSTERS) {
				weaponDamage =						54 * weaponDamageBonus;		//80 DPS
				weaponRange =						500;
				weaponFireRateMax =					20;
				weaponBulletSpeed =					10;
				weaponBulletRange =					-1;
				weaponBulletPierces =				false;
			}
			else if (weapon == Item.WEAPON_REPEATER) {
				weaponDamage =						13 * weaponDamageBonus;
				weaponRange =						500;
				weaponFireRateMax =					13;
				weaponBulletSpeed =					10;
				weaponBulletRange =					-1;
				weaponBulletPierces =				false;
			}
		}
		
		//Shoot a bullet at target if fire rate cooldown is up
		public function shoot() {
			//Decrement fire rate cooldown
			weaponFireRateCur--;
			
			//OPTIMIZATION CHECK
			//Only shoot if fire rate is ready and either (1) this is the partner (auto shoots) OR (2) this is the pc and the mouse is down
			if (optCounter % 3 == 0 && weaponFireRateCur <= 0 && (gameRoot.partner == this || (gameRoot.pc == this && shooting))) {
				weaponFireRateCur = weaponFireRateMax;	//reset cooldown
				
				//If this is the PC, just shoot a bullet
				if (gameRoot.pc == this) {
					var _bullet:Bullet = new Bullet(gameRoot, myParent, this, myParent.mouseX, myParent.mouseY);
				}
				//Otherwise, check for a target and shoot it
				else {
					//If no target OR current target is out of range OR target does not exist, select a new target 
					if (!target || gameRoot.getDistanceToPoint(x, y, target.x, target.y) > weaponRange || !gameRoot.contains(target)) {
						target = null;		//reset the previous target in case we don't happen to find a new target right now
						
						//1. Make a list of all targets that are in range
						var _potentialTargetList:Array = [];
						for (var i:int = 0; i < gameRoot.enemies.length; i++) {
							if (gameRoot.enemies[i].distanceToPartner <= weaponRange) _potentialTargetList.push(gameRoot.enemies[i]);
						}
						
						//2. As long as the list is at least 1 element long, choose a random target from within that list
						if (_potentialTargetList.length > 0) target = _potentialTargetList[int(Math.random()*_potentialTargetList.length)];
						
						//3. If it'_s not at least 1 element long, that means there are no potential targets, so we will wait for next frame to check again
						else return;
					}
					
					_bullet = new Bullet(gameRoot, myParent, this, target.x, target.y);
				}
			}
		}
		
		//Update image based on velocities
		private function UpdateGraphics() {
			if (gameRoot.pc == this) {
				var _rotDX:Number = myParent.mouseX - x;	//get relative location to mouse
				var _rotDY:Number = myParent.mouseY - y;
				
			}
			else {
				if (target) {
					_rotDX = target.x - x;					//get relative location to target
					_rotDY = target.y - y;
				}
				else {
					_rotDX = gameRoot.pc.x - x;				//get relative location to pc
					_rotDY = gameRoot.pc.y - y;
				}
			}
			
			rotation = Math.atan2(_rotDY, _rotDX) * (180 / Math.PI);
			
			if 		(rotation < -162.5) torso.gotoAndStop("w");
			else if (rotation < -117.5) torso.gotoAndStop("nw");
			else if (rotation < -72.5)	torso.gotoAndStop("n");
			else if (rotation < -27.5)	torso.gotoAndStop("ne");
			else if (rotation < 27.5)	torso.gotoAndStop("e");
			else if (rotation < 72.5)	torso.gotoAndStop("se");
			else if (rotation < 117.5)	torso.gotoAndStop("s");
			else if (rotation < 162.5)	torso.gotoAndStop("sw");
			else if (rotation > 162.5)	torso.gotoAndStop("w");
			
			if (dx == 0 && dy == 0) legs.gotoAndStop(torso.currentLabel);								//if not moving, reset legs and stop them
			else if (legs.currentLabel != torso.currentLabel) legs.gotoAndPlay(torso.currentLabel);		//if moving and legs are on wrong frame, set them to correct frame
			else legs.play();																			//keep legs playing through their animation
			
			if (torso.weaponGraphic) torso.weaponGraphic.gotoAndStop(torso.currentLabel);
			
			rotation = 0;
		}
		
		//Update position based on keys pressed and velocity, perform collision detection, and shoot target if possible
		private function UpdateVelocities() {
			//If this is the currently controlled character, update its velocities based on the keys being pressed
			if (gameRoot.pc == this) {
				var _n:Boolean = gameRoot.north;
				var _s:Boolean = gameRoot.south;
				var _e:Boolean = gameRoot.east;
				var _w:Boolean = gameRoot.west;
				
				if 		(_n && (!_w && !_e)) 	{dx = 0;		dy = -speed;	}	//just n
				else if (_n && _w) 				{dx = -speed;	dy = -speed / 2;}	//nw
				else if (_n && _e) 				{dx =  speed;	dy = -speed / 2;}	//ne
				else if (_s && (!_w && !_e))  	{dx = 0;		dy = speed; 	}	//just s
				else if (_s && _w) 				{dx = -speed;	dy = speed / 2; }	//sw
				else if (_s && _e) 				{dx = speed;	dy = speed / 2; }	//se
				else if (_e && (!_n && !_s)) 	{dx = speed; 	dy = 0;			}	//just e
				else if (_w && (!_n && !_s)) 	{dx = -speed; 	dy = 0;			}	//just w
				else dx = dy = 0;
				
				myParent.x -= dx;	//move level instead of pc right now
				myParent.y -= dy;
			}
			
			//Otherwise, if this is the partner, update movement based on the pc'_s position
			else {
				var _rotDX:Number = gameRoot.pc.x - x;				//get relative location to pc
				var _rotDY:Number = gameRoot.pc.y - y;
				var _rotAngle:Number = 360 * (Math.atan2(_rotDY, _rotDX) / (2 * Math.PI));
				
				dx = Math.cos(Math.PI * _rotAngle / 180) * speed;
				dy = Math.sin(Math.PI * _rotAngle / 180) * speed;
				
				//If too close to PC, stop
				if (gameRoot.getDistanceToPoint(x, y, gameRoot.pc.x, gameRoot.pc.y) < minRange) dx = dy = 0;
			}
			
			//Update character'_s position inside the level
			x += dx;
			y += dy;
		}
	}
}
﻿package chars {
	//IMPORTS
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.geom.Point;
	
	import chars.*;

	public class Enemy extends BaseCharacter {
		//PROPERTIES / Constants
		private const ZONE_MIN:int =	10;
		private const ZONE_MAX:int =	40;
		
		//PROPERTIES / Variables
		public var npValue:int;
		public var moveCheckTimerMax:int = 60;					//frames between move checks (30 FPS)
		public var moveCheckTimerCur:int = moveCheckTimerMax;
		
		//METHODS / Constructor
		public function Enemy(rootRef:MovieClip, displayParent:MovieClip, characterType:int) {
			super(rootRef, displayParent, characterType);
			
			//New methods
			SetWeaponParams();
			
			//Inherited properties
				stunIcon.visible = false;
				confuseIcon.visible = false;
				
				//Determine spawn zone
				var _randZone:int = Math.random() * 4;
				var _randHorizontal:int = Math.random() * gameRoot.RIGHT_EDGE;
				var _randVertical:int = Math.random() * gameRoot.BOTTOM_EDGE;
				var _leftEdge:int = gameRoot.pc.x - gameRoot.RIGHT_EDGE / 2;
				var _rightEdge:int = -_leftEdge;
				var _topEdge:int = gameRoot.pc.y - gameRoot.BOTTOM_EDGE / 2;
				var _bottomEdge:int = -_topEdge;
				
				if 		(_randZone == 0) { x = _leftEdge - 50; 		y = _randVertical; 		}
				else if (_randZone == 1) { x = _randHorizontal;		y = _topEdge - 50; 		}
				else if (_randZone == 2) { x = _rightEdge + 50;		y = _randVertical;		}
				else if (_randZone == 3) { x = _randHorizontal;		y = _bottomEdge + 50;	}
			
			//Inherited methods
			stunIcon.stop();
			confuseIcon.stop();
			gameRoot.enemies.push(this);
			//addFrameScript(charType, function() { torso.stop(); legs.stop(); } );
			
			//Type-specific setup
			if 		(currentFrame == gameRoot.ENEMY_RADISH) 			{ speed = 2; 	healthMax = 100; 	npValue = 10; }
			else if (currentFrame == gameRoot.ENEMY_BROCCOLI) 			{ speed = 2;	healthMax = 200; 	npValue = 13; minRange = 10; }
			else if (currentFrame == gameRoot.ENEMY_BRUSSELS) 			{ speed = 3;	healthMax = 250; 	npValue = 15; }
			else if (currentFrame == gameRoot.ENEMY_CORN) 				{ speed = 5;	healthMax = 300; 	npValue = 20; minRange = 10; }
			else if (currentFrame == gameRoot.ENEMY_EGGPLANT)			{ speed = 3;	healthMax = 350;	npValue = 22; }
			else if (currentFrame == gameRoot.ENEMY_CAULI)				{ speed = 4;	healthMax = 380;	npValue = 25; }
			else if (currentFrame == gameRoot.ENEMY_PEPPER)				{ speed = 3;	healthMax = 400;	npValue = 27; }
			else if (currentFrame == gameRoot.ENEMY_SPINACH)			{ speed = 6;	healthMax = 420;	npValue = 30; minRange = 10; }
			else if (currentFrame == gameRoot.ENEMY_SQUASH)				{ speed = 5;	healthMax = 440;	npValue = 32; }
			else if (currentFrame == gameRoot.ENEMY_MUSHROOM)			{ speed = 5;	healthMax = 500;	npValue = 35; }
			
			else if	(currentFrame == gameRoot.ENEMY_FLOWER_CLOWN) 		{ speed = 2; 	healthMax = 100; 	npValue = 10; }
			else if (currentFrame == gameRoot.ENEMY_MIME) 				{ speed = 2;	healthMax = 200; 	npValue = 13; }
			else if (currentFrame == gameRoot.ENEMY_LION) 				{ speed = 3;	healthMax = 250; 	npValue = 15; }
			else if (currentFrame == gameRoot.ENEMY_FIREBLOWER) 		{ speed = 5;	healthMax = 300; 	npValue = 20; }
			else if (currentFrame == gameRoot.ENEMY_SNEAKY_CLOWN)		{ speed = 3;	healthMax = 350;	npValue = 22; }
			else if (currentFrame == gameRoot.ENEMY_JUGGLING_CLOWN)		{ speed = 4;	healthMax = 380;	npValue = 25; }
			else if (currentFrame == gameRoot.ENEMY_VENTRILOQUIST)		{ speed = 3;	healthMax = 400;	npValue = 27; }
			else if (currentFrame == gameRoot.ENEMY_EVIL_BALLOON)		{ speed = 6;	healthMax = 420;	npValue = 30; }
			else if (currentFrame == gameRoot.ENEMY_DEVIOUS_CLOWN)		{ speed = 5;	healthMax = 440;	npValue = 32; }
			else if (currentFrame == gameRoot.ENEMY_RINGLEADER)			{ speed = 5;	healthMax = 500;	npValue = 35; }
			
			else trace("Invalid enemy type passed to Enemy constructor.");
			
			gameRoot.modifyUnitHealth(this, healthMax);	//fill life
		}
		
		
		//METHODS / Destroy Self
		public function killMe(killer:MovieClip = null) {
			gameRoot.enemies.splice(gameRoot.enemies.indexOf(this), 1);		//remove self from Main's list of enemies
			myParent.removeChild(this);
			
			if (killer is PC) {
				//GENERATE RANDOM ITEM DROP
				
				//var _poofFader:Fader = new Fader(gameRoot, myParent, Fader.FADER_POOF, x, y);
				//_poofFader = new Fader(gameRoot, myParent, Fader.FADER_POOF, x + 20, y - 30);
				////_poofFader = new Fader(gameRoot, myParent, Fader.FADER_POOF, x - 30, y);
				
				var _goreExplosion:GoreExplosion = new GoreExplosion(gameRoot, myParent, x, y);
				
				//gameRoot.shakeScreen();
				
				//Only award NP if killed by PC
				if (killer == gameRoot.pc) {
					var npAwarded:int = npValue * gameRoot.npMultKillEnemy;
					
					gameRoot.np += npAwarded;			//award player with nightmare points
					gameRoot.npThisWave += npAwarded;	//track nightmare points gained during each wave
					
					var _pointBurst:Fader = new Fader(gameRoot, myParent, Fader.FADER_TEXT, x, y, (npAwarded + " NP").toString());
				}
				
				gameRoot.checkWaveComplete();
			}
		}
		
		
		//Update position based on velocity, and shoot target if possible
		public function pulse() {
			optCounter++;
			
			//Limit actions based on debuffs
			if (stunned) {
				stunIcon.visible = true;
				stunIcon.play();
				legs.stop();			//legs and torso freeze
				torso.stop();
			}
			else if (confused) {
				confuseIcon.visible = true;
				confuseIcon.play();
				torso.play();			//legs and torso spin around
				legs.play();
				
				Shoot();
			}
			else {
				stunIcon.visible = confuseIcon.visible = false;
				stunIcon.stop();
				confuseIcon.stop();
				
				UpdateRanges();
				UpdateVelocities();
				Shoot();
				UpdateGraphics();
			}
		}
		
		//Create weapon stats based on character type
		protected override function SetWeaponParams() {
			weapon = currentLabel;
			
			if 		(currentFrame == gameRoot.ENEMY_RADISH) {
				weaponDamage =					5;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_BROCCOLI) {
				weaponDamage =					7;
				weaponRange =					30;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_BRUSSELS) {
				weaponDamage =					4;
				weaponRange =					500;
				weaponFireRateMax =				30;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_CORN) {
				weaponDamage =					10;
				weaponRange =					30;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_EGGPLANT) {
				weaponDamage =					13;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				7;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_CAULI) {
				weaponDamage =					4;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			true;
			}
			else if (currentFrame == gameRoot.ENEMY_PEPPER) {
				weaponDamage =					20;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_SPINACH) {
				weaponDamage =					6;
				weaponRange =					30;
				weaponFireRateMax =				15;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_SQUASH) {
				weaponDamage =					40;
				weaponRange =					500;
				weaponFireRateMax =				90;
				weaponBulletSpeed =				10;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_MUSHROOM) {
				weaponDamage =					30;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			
			else if	(currentFrame == gameRoot.ENEMY_FLOWER_CLOWN) {
				weaponDamage =					8;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				8;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_MIME) {
				weaponDamage =					6;
				weaponRange =					500;
				weaponFireRateMax =				30;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_LION) {
				weaponDamage =					4;
				weaponRange =					30;
				weaponFireRateMax =				15;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_FIREBLOWER) {
				weaponDamage =					2.5;
				weaponRange =					500;
				weaponFireRateMax =				7;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			true;
			}
			else if (currentFrame == gameRoot.ENEMY_SNEAKY_CLOWN) {
				weaponDamage =					25;
				weaponRange =					700;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				9;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_JUGGLING_CLOWN) {
				weaponDamage =					14;
				weaponRange =					500;
				weaponFireRateMax =				30;
				weaponBulletSpeed =				10;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_VENTRILOQUIST) {
				weaponDamage =					35;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_EVIL_BALLOON) {
				weaponDamage =					20;
				weaponRange =					30;
				weaponFireRateMax =				30;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_DEVIOUS_CLOWN) {
				weaponDamage =					24;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				4;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
			else if (currentFrame == gameRoot.ENEMY_RINGLEADER) {
				weaponDamage =					54;
				weaponRange =					500;
				weaponFireRateMax =				60;
				weaponBulletSpeed =				5;
				weaponBulletRange =				-1;
				weaponBulletPierces =			false;
			}
		}
		
		//Shoot target if possible
		private function Shoot() {
			weaponFireRateCur--;
			
			//OPTIMIZATION CHECK
			if (optCounter % 3 && weaponFireRateCur <= 0) {
				weaponFireRateCur = weaponFireRateMax;	//reset weapon cooldown
				
				//If confused, shoot at random enemy
				if (confused) {
					var _randomEnemy:Enemy = gameRoot.enemies[int(Math.random() * gameRoot.enemies.length)];
					
					var _bullet:Bullet = new Bullet(gameRoot, gameRoot.level, this, _randomEnemy.x, _randomEnemy.y);
				}
				//Otherwise, in range of weapon then attack
				else if (distanceToPC <= weaponRange) {
					_bullet = new Bullet(gameRoot, gameRoot.level, this, gameRoot.pc.x, gameRoot.pc.y);
				}
			}
		}
		
		//Update image based on velocities
		private function UpdateGraphics() {
			if (torso) {
				var _rotDX:Number = gameRoot.pc.x - x;				//get relative location to pc
				var _rotDY:Number = gameRoot.pc.y - y;
				
				rotation = Math.atan2(_rotDY, _rotDX) * (180 / Math.PI);
				
				if 		(rotation < -162.5) torso.gotoAndStop("w");
				else if (rotation < -117.5) torso.gotoAndStop("nw");
				else if (rotation < -72.5)	torso.gotoAndStop("n");
				else if (rotation < -27.5)	torso.gotoAndStop("ne");
				else if (rotation < 27.5)	torso.gotoAndStop("e");
				else if (rotation < 72.5)	torso.gotoAndStop("se");
				else if (rotation < 117.5)	torso.gotoAndStop("s");
				else if (rotation < 162.5)	torso.gotoAndStop("sw");
				else if (rotation > 162.5)	torso.gotoAndStop("w");
				
				if (dx == 0 && dy == 0) legs.gotoAndStop(torso.currentLabel);								//if not moving, reset legs and stop them
				else if (legs.currentLabel != torso.currentLabel) legs.gotoAndPlay(torso.currentLabel);		//if moving and legs are on wrong frame, set them to correct frame
				else legs.play();																			//keep legs playing through their animation
				
				if (torso.weaponGraphic) torso.weaponGraphic.gotoAndStop(torso.currentLabel);
				
				rotation = 0;
			}
		}
		
		//Calculate distance from this unit to other units
		private function UpdateRanges() {
			//OPTIMIZATION CHECK
			if (optCounter % 5 == 0) {
				distanceToPC = gameRoot.getDistanceToPoint(x, y, gameRoot.pc.x, gameRoot.pc.y);
				if (gameRoot.partner) distanceToPartner = gameRoot.getDistanceToPoint(x, y, gameRoot.partner.x, gameRoot.partner.y);
			}
		}
		
		//Tend to keep doing what this was doing, but set velocities based on distance to target and random chance to stop moving when possible
		private function UpdateVelocities() {
			moveCheckTimerCur--;
			
			//If outside range, flag for movement
			if (distanceToPC > weaponRange) dx = 1;
			
			//Otherwise, if timer finished, potentially flag for stop
			else if (moveCheckTimerCur <= 0) {
				moveCheckTimerCur = moveCheckTimerMax;
				if (Math.random() < .5) dx = dy = 0;
				else dx = 1;
			}
			
			//Only update true velocities if flagged for movement
			if (dx > 0 || dy > 0) {
				var _rotDX:Number = gameRoot.pc.x - x;				//get relative location to pc
				var _rotDY:Number = gameRoot.pc.y - y;
				var _rotAngle:Number = 360 * (Math.atan2(_rotDY, _rotDX) / (2 * Math.PI));
				
				dx = Math.cos(Math.PI * _rotAngle / 180) * speed;
				dy = Math.sin(Math.PI * _rotAngle / 180) * speed;
				
				x += dx;
				y += dy;
			}
		}
	}
}
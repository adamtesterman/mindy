﻿package chars {
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.filters.GlowFilter;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class BaseCharacter extends MovieClip {
		//PROPERTIES / Constants
		protected const START_X:int =						0;		//pc start positions
		protected const START_Y:int =						0;
		protected const START_FACING:String =				"s";
		
		public static const SKILL_BRAT:String =				"brat";
		public static const SKILL_CRYBABY:String =			"crybaby";
		public static const SKILL_GRIZZLY:String =			"grizzly";
		public static const SKILL_HYPNOSIS:String =			"hypnosis";
		
		//PROPERTIES / Variables
		public var dx:Number = 0;							//movement velocities
		public var dy:Number = 0;
		public var speed:Number;
		public var defense:Number = 1;						//serves as a percentage of damage reduction (1 = 100% of damage taken, .5 = 50% of damage taken, 1.2 = 20% EXTRA damage taken, etc.)
		public var healthMax:int;
		public var healthCur:int;
		public var dead:Boolean;							//determines whether or not this character can act
		public var energyMax:int = 100;
		public var energyCur:int;
		public var confused:Boolean;
		public var stunned:Boolean;
		
		public var weapon:String;							//weapon parameters
		public var weaponDamage:int;
		public var weaponDamageBonus:Number = 1;			//as a percent (1 = 100% of damage dealt, .5 = 50% of damage dealth, 1.2 = 20% EXTRA damage dealt, etc.)
		public var weaponRange:int;
		public var weaponFireRateMax:int;
		public var weaponFireRateCur:int;
		public var weaponBulletSpeed:int;
		public var weaponBulletRange:int;
		public var weaponBulletPierces:Boolean;
		protected var currentWeaponIndex:int;
		
		public var distanceToPC:int;
		public var distanceToPartner:int;
		protected var minRange:int = 100;					//can't get any closer to target/partner than this (in pixels)
		protected var optCounter:int;						//optimization counter (prevents certain tasks from being performed every single frame)
		
		//PROPERTIES / Objects
		protected var gameRoot:MovieClip;					//reference to Main
		protected var myParent:MovieClip;					//parent object (usually Main)
		public var target:MovieClip;
		
		//PROPERTIES / Lists
		public static var skillPool:Array = [];				//list of all skills in the game
		protected var activeSkills:Array = [];				//list of all skills that are currently in effect, with their timers counting down	
		public var invWeapons:Array = [];					//list of all weapons in inventory
		public var invSkills:Array = [];					//list of all skills in inventory
		
		//FILL SKILL PARAMETERS
		skillPool[SKILL_BRAT] = new Object();
		skillPool[SKILL_BRAT].name = 					SKILL_BRAT;
		skillPool[SKILL_BRAT].recastTimerCur =			0;
		skillPool[SKILL_BRAT].recastTimerMax =			6300;				//3.5 min
		skillPool[SKILL_BRAT].buffTimerCur =			0;
		skillPool[SKILL_BRAT].buffTimerMax =			600;				//20 sec
		skillPool[SKILL_BRAT].debuffTimerCur =			0;
		skillPool[SKILL_BRAT].debuffTimerMax =			0;					//no debuff timer
		skillPool[SKILL_BRAT].inventoryGroup =			"mindySkillSlot";
		
		skillPool[SKILL_CRYBABY] = new Object();
		skillPool[SKILL_CRYBABY].name =					SKILL_CRYBABY;
		skillPool[SKILL_CRYBABY].recastTimerCur =		0;
		skillPool[SKILL_CRYBABY].recastTimerMax =		6300;				//3.5 min
		skillPool[SKILL_CRYBABY].buffTimerCur =			0;
		skillPool[SKILL_CRYBABY].buffTimerMax =			450;				//15 sec
		skillPool[SKILL_CRYBABY].debuffTimerCur =		0;
		skillPool[SKILL_CRYBABY].debuffTimerMax =		600;				//20 sec
		skillPool[SKILL_CRYBABY].inventoryGroup =		"mindySkillSlot";
		
		skillPool[SKILL_GRIZZLY] = new Object();
		skillPool[SKILL_GRIZZLY].name =					SKILL_GRIZZLY;
		skillPool[SKILL_GRIZZLY].recastTimerCur =		0;
		skillPool[SKILL_GRIZZLY].recastTimerMax =		6300;				//3.5 min
		skillPool[SKILL_GRIZZLY].buffTimerCur =			0;
		skillPool[SKILL_GRIZZLY].buffTimerMax =			600;				//20 sec
		skillPool[SKILL_GRIZZLY].debuffTimerCur =		0;
		skillPool[SKILL_GRIZZLY].debuffTimerMax =		0;					//no debuff timer
		skillPool[SKILL_GRIZZLY].inventoryGroup =		"binkySkillSlot";
		
		skillPool[SKILL_HYPNOSIS] = new Object();
		skillPool[SKILL_HYPNOSIS].name =				SKILL_HYPNOSIS;
		skillPool[SKILL_HYPNOSIS].recastTimerCur =		0;
		skillPool[SKILL_HYPNOSIS].recastTimerMax =		6300;				//3.5 min
		skillPool[SKILL_HYPNOSIS].buffTimerCur =		0;
		skillPool[SKILL_HYPNOSIS].buffTimerMax =		450;				//15 sec
		skillPool[SKILL_HYPNOSIS].debuffTimerCur =		0;
		skillPool[SKILL_HYPNOSIS].debuffTimerMax =		750;				//25 sec
		skillPool[SKILL_HYPNOSIS].inventoryGroup =		"binkySkillSlot";
		
		
		//METHODS / Constructor
		public function BaseCharacter(rootRef:MovieClip, displayParent:MovieClip, characterType:int) {
			//New properties
			gameRoot = rootRef;
			myParent = displayParent;
			
			//Inherited properties
			scaleX = scaleY = .5;		//start at half size (CHANGE LATER)
			
			//Inherited methods
			gotoAndStop(characterType);
			myParent.addChild(this);
		}
		
		
		//OVERRIDE METHODS / Set Weapon Parameters
		protected function SetWeaponParams() {
			
		}
		
		
		//Activate the effect of a Nightmare Skill. Returns whether or not skill activation was successful
		public function activateSkill(skill:String):Boolean {
			//Check if alive
			if (dead)													return false;
			//Check user's energy pool
			else if	(energyCur < energyMax) 							return false;
			//Check if skill is already active (still cooling down)
			else if (CheckActiveSkill(skill))							return false;
			//Check global skill cooldown
			else if (gameRoot.globalRecastTimerCur > 0)					return false;
			//Continue with skill activation
			else {
				trace("activating skill: " + skill);	
				
				//Player feedback
				var glowFilter:GlowFilter = new GlowFilter;
				glowFilter.blurX = glowFilter.blurY = 10;
				
				//Apply the skill's buff and debuff effects
				if 		(skill == BaseCharacter.SKILL_BRAT) {
					//Buff effects
					speed *= 1.5;					//+50% speed
					defense *= 1.5;					//+50% defense
					weaponDamageBonus = 1.5;		//+50% damage
					equipWeapon(weapon);			//update weapon stats to reflect new damage bonus
					
					//Debuff effects
					gameRoot.modifyUnitHealth(this, -(int(.3 * healthCur)));	//-30% current health
					
					//PLAYER FEEDBACK
					glowFilter.color = 0xFF0000;	//red
				}
				else if	(skill == BaseCharacter.SKILL_CRYBABY) {
					//Buff effects
					for (var i:int = 0; i < gameRoot.enemies.length; i++) {		//stun all enemies
						gameRoot.enemies[i].stunned = true;
					}
					gameRoot.pauseEnemySpawns = true;	//temporarily stop enemies from spawning
					
					//Debuff effects
					speed *= .85;					//-15% speed
					defense *= .85;					//-15% defense
					weaponDamageBonus = .85;		//-15% damage
					equipWeapon(weapon);			//update weapon stats to reflect new damage bonus
					
					//PLAYER FEEDBACK
					glowFilter.color = 0x0000FF;	//blue
				}
				else if (skill == BaseCharacter.SKILL_GRIZZLY) {
					//Buff effects
					speed *= 1.5;					//+50% speed
					defense *= 1.5;					//+50% defense
					weaponDamageBonus = 1.5;		//+50% damage
					equipWeapon(weapon);			//update weapon stats to reflect new damage bonus
					
					//Debuff effects
					gameRoot.modifyUnitHealth(this, -(int(.3 * healthCur)));	//-30% current health
					
					//PLAYER FEEDBACK
					glowFilter.color = 0xFFFF00;		//yellow
				}
				else if (skill == BaseCharacter.SKILL_HYPNOSIS) {
					//Buff effects
					for (i = 0; i < gameRoot.enemies.length; i++) {				//confuse all enemies
						gameRoot.enemies[i].confused = true;
					}
					gameRoot.pauseEnemySpawns = true;	//temporarily stop enemies from spawning
					
					//Debuff effects
					speed *= .75;					//-25% speed
					defense *= .75;					//-25% defense
					weaponDamageBonus = .75;		//-25% damage
					equipWeapon(weapon);			//update weapon stats to reflect new damage bonus
					
					//PLAYER FEEDBACK
					glowFilter.color = 0xFF00FF;	//fuschia
				}
				
				//Remove 100% of the user's energy
				gameRoot.modifyUnitEnergy(this, -energyCur);
				
				//Remove the skill from the user's inventory
				gameRoot.discardItem(skill, this);
				
				//Start the global skill cooldown
				gameRoot.globalRecastTimerCur = gameRoot.GLOBAL_RECAST_TIMER_MAX;
				
				//Add this skill to the user's active skills so its cooldown timers can be tracked
				AddActiveSkill(skill);
				
				//Begin counting down all recast timers
				addEventListener(Event.ENTER_FRAME, UpdateSkillTimers);
				
				//Player feedback
				filters = [glowFilter];
				
				return true;
			}
		}
		
		//Add a skill to the list of active skills so that its cooldown timers can be tracked
		private function AddActiveSkill(skill:String) {
			var _newSkill:Object = new Object();
			_newSkill.name = skillPool[skill].name;
			_newSkill.recastTimerMax = skillPool[skill].recastTimerMax;
			_newSkill.recastTimerCur = _newSkill.recastTimerMax;
			_newSkill.buffTimerMax = skillPool[skill].buffTimerMax;
			_newSkill.buffTimerCur = _newSkill.buffTimerMax;
			_newSkill.debuffTimerMax = skillPool[skill].debuffTimerMax;
			_newSkill.debuffTimerCur = _newSkill.debuffTimerMax;
			_newSkill.inventoryGroup = skillPool[skill].inventoryGroup;
			
			activeSkills.push(_newSkill);
		}
		
		//Checks if a certain skill is still cooling down and returns the result
		private function CheckActiveSkill(skillName:String):Boolean {
			for (var i:int = 0; i < activeSkills.length; i++) {
				if (activeSkills[i].name == skillName && activeSkills[i].recastTimerCur > 0) return true;
			}
			
			return false;
		}
		
		//Ends the effects of all skills that this character has applied
		//True should be passed upon quitting the game, but false should be passed when a character dies
		public function deactivateSkills(resetRecastTimer:Boolean) {
			for (var i:int = activeSkills.length - 1; i >= 0; i--) {
				trace("deactivating: " + activeSkills[i].name);
				if (resetRecastTimer) activeSkills[i].recastTimer = 0;
				activeSkills[i].buffTimer = 0;
				activeSkills[i].debuffTimer = 0;
				
				HandleBuffTimerComplete(activeSkills[i]);
				HandleDebuffTimerComplete(activeSkills[i]);
				
				if (resetRecastTimer) activeSkills.splice(i, 1);
			}
		}
		
		//Switch to next weapon in weapon list
		public function equipNextWeapon() {
			//If there is a next weapon, switch to it. Otherwise, go back to the first weapon.
			if (invWeapons[currentWeaponIndex + 1]) equipWeapon(invWeapons[currentWeaponIndex + 1]);
			else equipWeapon(invWeapons[0]);
		}
		
		//Switch to previous weapon in weapon list
		public function equipPrevWeapon() {
			//If there is a previous weapon, switch to it. Otherwiise, go to the last weapon.
			if (invWeapons[currentWeaponIndex - 1]) equipWeapon(invWeapons[currentWeaponIndex - 1]);
			else equipWeapon(invWeapons[invWeapons.length - 1]);
		}
		
		//Switch to a specific weapon in weapon list
		public function equipWeapon(newWeapon:String) {
			weapon = newWeapon;
			
			SetWeaponParams();
		}
		
		//Perform certain actions when a skill's buff timer has ended
		private function HandleBuffTimerComplete(skill:Object) {
			//Only remove the effects if the timer is still going. Otherwise, it means the timer had already completed, so we don't want to do this more than once.
			if (skill.buffTimerCur > 0) {
				skill.buffTimerCur = 0;
				
				//Remove the skill's buff effects
				if 		(skill.name == BaseCharacter.SKILL_BRAT) {
					//Buff effects
					speed /= 1.5;					//-50% speed
					defense /= 1.5;					//-50% defense
					weaponDamageBonus = 1;			//-50% damage
					equipWeapon(weapon);			//update weapon stats to reflect new damage bonus
				}
				else if	(skill.name == BaseCharacter.SKILL_CRYBABY) {
					//Buff effects
					for (var i:int = 0; i < gameRoot.enemies.length; i++) {
						gameRoot.enemies[i].stunned = false;
					}
					gameRoot.pauseEnemySpawns = false;
				}
				else if (skill.name == BaseCharacter.SKILL_GRIZZLY) {
					//Buff effects
					speed /= 1.5;					//-50% speed
					defense /= 1.5;					//-50% defense
					weaponDamageBonus = 1;			//-50% damage
					equipWeapon(weapon);			//update weapon stats to reflect new damage bonus
				}
				else if (skill.name == BaseCharacter.SKILL_HYPNOSIS) {
					//Buff effects
					for (i = 0; i < gameRoot.enemies.length; i++) {
						gameRoot.enemies[i].confused = false;
					}
					gameRoot.pauseEnemySpawns = false;
				}
				
				trace(skill.name + " buff effects removed");
				
				//PLAYER FEEDBACK
				filters = [];
			}
		}
		
		//Perform certain actions when a skill's debuff timer has ended
		private function HandleDebuffTimerComplete(skill:Object) {
			//Only remove the effects if the timer is still going. Otherwise, it means the timer had already completed, so we don't want to do this more than once.
			if (skill.debuffTimerCur > 0) {
				skill.debuffTimerCur = 0;
				
				//Remove the skill's debuff effects
				if 		(skill.name == BaseCharacter.SKILL_CRYBABY) {
					//Debuff effects
					speed /= .85;					//+15% speed
					defense /= .85;					//+15% defense
					weaponDamageBonus = 1;			//+15% damage
					equipWeapon(weapon);			//update weapon stats to reflect new damage bonus
				}
				else if (skill.name == BaseCharacter.SKILL_HYPNOSIS) {
					//Debuff effects
					speed /= .75;					//+25% speed
					defense /= .75;					//+25% defense
					weaponDamageBonus = 1;			//+25% damage
					equipWeapon(weapon);			//update weapon stats to reflect new damage bonus
				}
				
				trace(skill.name + " debuff effects removed");
				
				//PLAYER FEEDBACK
			}
		}
		
		//Continuously track cooldown timers on each active skill
		private function UpdateSkillTimers(event:Event = null) {
			//Only update cooldowns while game is being played
			if (gameRoot.gameState == gameRoot.GAME_PLAYING) {
				//Decrement cooldowns only for skills that are cooling down
				for (var i:int = activeSkills.length - 1; i >= 0; i--) {
					var _thisSkill:Object = activeSkills[i];
					
					if (_thisSkill.recastTimerCur > 0) {
						//Decrement recast timers
						_thisSkill.recastTimerCur--;
						if (_thisSkill.recastTimerCur < 0) _thisSkill.recastTimerCur = 0;
						
						//trace(_thisSkill.name + " recast: " + _thisSkill.recastTimerCur);
						
						//Decrement buff timers and handle timer completion if needed
						_thisSkill.buffTimerCur--;															//decrement the timer
						if (_thisSkill.buffTimerCur == 1) HandleBuffTimerComplete(_thisSkill);				//if finished, handle timer complete
						else if (_thisSkill.buffTimerCur < 0) _thisSkill.buffTimerCur = 0;					//otherwise, if finished previously, keep timer at 0
						
						//trace(_thisSkill.name + " buff timer: " + _thisSkill.buffTimerCur);
						
						//Decrement debuff timers and handle timer completion if needed
						_thisSkill.debuffTimerCur--;														//decrement the timer
						if (_thisSkill.debuffTimerCur == 1) HandleDebuffTimerComplete(_thisSkill);			//if finished, handle timer complete
						else if (_thisSkill.debuffTimerCur < 0) _thisSkill.debuffTimerCur = 0;				//otherwise, if finished previously, keep timer at 0
						
						//trace(_thisSkill.name + " debuff timer: " + _thisSkill.debuffTimerCur);
					}
					
					//Check to see if all of this skill's cooldowns are finished
					if (_thisSkill.recastTimerCur == 0 && _thisSkill.buffTimerCur == 0 && _thisSkill.debuffTimerCur == 0) {
						activeSkills.splice(activeSkills.indexOf(_thisSkill), 1);	//remove it from list of active skills so it won't be cooled down anymore
						_thisSkill = null;
					}
				}
				
				//Stop updating all skill timers if they're all complete
				if (activeSkills.length == 0) removeEventListener(Event.ENTER_FRAME, UpdateSkillTimers);	
			}
		}
	}
}
#include "DarkGDK.h"
#include <string.h>
	//Declare Constants
	//Declare Variables
	//Function Prototypes
	//Display Title
void DarkGDK ()
{
	//Main Game Loop
		//ScreenUpdate()
		//Button_NewCharacter
			//OnMouseOver - highlight
			//OnMouseLeftClick - NewCharacter()
		//Button_LoadCharacter -
			//OnMouseOver - highlight
			//OnMouseLeftClick - popup scroll window showing all saved characters
				//OnCondition(menu option highlighted with mouse)OnMouseLeftClick - Load saved character data into memory
		//Button_Loadgame
			//OnCondition(no character selected)OnMouseOver - no highlight
			//OnCondition(a character has been selected)OnMouseOver - highlight button
			//OnCondition(a character has been selected)OnLeftMouseClick - popup scroll window listing of saves
				//OnCondition(mouse pointer moves to edge of save graphic)OnMouseOver - scroll saves list in that direction
				//OnLeftMouseClick - load target save data into memory
		//Button_MissionSelect
			//OnCondition(no character selected)MouseOver - No highlight
			//OnCondition(a character has been selected)OnMouseOver - Highlight
			//OnCondition(a character has been selected)OnMouseLeftClick - open popup window listing mission names
				//OnConditon(Mouse pointer moves to edge of mission list)OnMouseOver - scroll mission list in that direction
			//OnCondition(mouse pointer higlighting misson)OnLeftMouseClick
				//Load Board components
				//Load opponents
				//Load all decks
				//Load Player Data
				//Intro story based on mission
				//Board Fly Over - Move screen position from end of board down path to start
					//store a set of x,y coords in array that way we can expand basic board Size
				//Call Main Board Loop
		//Button_GameOptions
		//Button_ConfigureDeck
			//OnCondition(no character selected)MouseOver - No highlight
			//OnCondition(a character has been selected)OnMouseOver - Highlight
			//OnCondition(a character has been selected)OnMouseLeftClick - open popup window listing deck names
				//OnConditon(Mouse pointer moves to edge of deck list)OnMouseOver - scroll deck list in that direction
		//Button_GuildTutorials - 
			//OnMouseOver - highlight
			//OnMouseLeftClick - opens a popup window showing a list of turorials(guild lessons) you can view
			//OnCondition(mouse pointed at a tutorial listing)OnMouseLeftClick - open selected tutorial
}

//Board Start - Main Board Loop
	//while (!dbKey()) need to have it check an unused key
	//{
		//GameState=MainGameState
		//ScreenUpdate()
			//Change board to show new path (higlight path and leave rest dark)
			//each square should have four data fields for pathing logic should be
				//Check entrance direction to square, exit direction then equals
				//Turn on highlight flag for each square in path up to set number maybe 10(may be affected by player/item?)
		//Update Sound (Background Music)
		//Button_RollToMove  
			//OnLeftMouseClick - RollDice ()
			//Display BoardMove Background, repeat walk cycle equal to dice result, scroll background
			//Execute Ending squares effects (in the following order)
				//Monster 
					//Determine monster type by board area
					//Play Monster Card rotation animation
					//Send info into BattleGameLoop()
				//Trap
					//Determine trap type by board area
					//Play Trap Card rotation animation
					//Send TrapCode to TrapExecute()
				//Treasure
					//Determine treasure type by board area
					//Play Treasure Card rotation animation
					//Add item to players inventory
				//Enter Shop
					//Load Shop Inventory by ID
					//Send to ShopGameLoop()
		//Button_DisplayAbilityList - Main Screen
		//Button_DisplayInventory - Main Screen
		//Button/Hotkey - SaveGame (if hotkey maybe main game loop)
	//}

//Battle Game Loop() (Play all attacks and abilities,end turn,draw)
	/*
	{
		//ScreenUpdate()
		//GameState=BattleGameState
		//Button_DisplayAbilityList (reference main screen or battle, highlight useable items)
		//Button_DisplayInventory (reference main screen or battle, highlight useable items)
		//Button_CardSlot# 
			//OnLeftMouseClick
				//display full rotation for card with pause
					//OptionSetting - quicker cards  Rotation is speeded up to quicken gameplay
				//carry out effect of card
			//OnMouseOver - display cards rotation in
			//OnMouseOff - display cards rotation out
			//OnCondition(attack card clicked)
				//Allow defender to select defense card
				//Determine attack hit or miss
		//Button_ScrollCardHand left&right
		//OnCondition(player ends turn)
			//Move to Creatures Turn
				//Creature Abilities
				//Creature Inventory
				//Creature Attack
	}*/

//Shop Game Loop()
	/*
	{	
		//ScreenUpdate()
		//Load Shop inventory & player inventory
		//Shop and player inventory be linked to buy & sell buttons
		//Shop Inventory (Opens on)
			//OnMouseOver		higlight item & show item_tooltip_price
			//OnMouseLeftClick	highlight buy button & item
			//OnMouseShiftLeftClick buy item and move to player inventory
				//refund if no space and display inventory full notice
			//Button_BuyTab		
				//OnCondition(item clicked)OnLeftMouseClick
					//Buy item & add to player inventory
					//Lower shop count or remove from shop inventory
					//refund if no space and display inventory full notice
				//OnCondition(item clicked)OnMouseOver - display item_tooltip_price
			//Button_SellTab	Change screen to show player inventory
				//OnMouseOver	Highlight
		//Player Inventory
			//onMouseOver highlight item & show tooltip value
			//OnMouseLeftClick	highlight sell button
			//OnMouseShiftLeftClick sell item & remove from player inventory
			//Button_BuyTab		Change screen to show shop inventory
				//OnMouseOver	Highlight
			//Button_SellTab	
				//OnCondition(item clicked)OnLeftMouseClick - Sell item & remove from inventory
				//refund if no space and display inventory full notice
				//OnCondition(item clicked)OnMouseOver - display item_tooltip_price
	}*/

/*
ScreenUpdate()
{
	//Display Background & Overleaf
	//Display Menus
	//Display Character & Beasts
}*/

/*
RollDice()
{
	//Should generate multiple dice results and show them
	//Generation should slow then stop
	//Output result
}*/

/*
BattleDeckExecution()
{
	//Refer to CardCode array and execute effect appropriately
}*/

/*
InventoryExecution()
{
	//Refer to ItemCode array and execute effect appropriately
}*/

/*
AbilityExecution()
{
	//Refer to AbilityCode array and execute effect appropriately
}*/
/*
TrapExecution()
{
	//Refer to TrapCode array and execute effect appropriately
}*/